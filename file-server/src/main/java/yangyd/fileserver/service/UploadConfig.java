package yangyd.fileserver.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * Configure a RestTemplate for streaming multipart message.
 * The default Spring implementation doesn't properly handle stream that's not originated from a file.
 * This implementation, along with the {@link yangyd.anthology.web.NamedInputStreamResource} is able to give a name
 * to the stream (so it looks like a file to the server)
 * and prevents RestTemplate from trying to calculate content-length from the stream (which doesn't make sense for streaming).
 * </p>
 *
 * <p>
 * NamedInputStreamResource actually contains a hack that solves the content-length problem so you may not need all these mess, but anyway this example
 * shows how to customize message converters for the RestTemplate.
 * </p>
 */
@Configuration
public class UploadConfig {

  @Bean("multipart")
  RestTemplate multipartTemplate() {
    // Replace the default ResourceHttpMessageConverter with our implementation
    // which don't set content-length header as we don't have that for inputStream.
    // (trying to calculate the length will cause problem)
    RestTemplate restTemplate = new RestTemplate();
    for (HttpMessageConverter<?> c : restTemplate.getMessageConverters()) {
      if (c instanceof FormHttpMessageConverter) {
        ((FormHttpMessageConverter) c).setPartConverters(converters());
      }
    }
    return restTemplate;
  }

  /**
   * Assemble converter stack for streaming multipart message. Besides apply the customized StreamingMessageConverter, it simply mimics the default implementation.
   */
  private List<HttpMessageConverter<?>> converters() {
    List<HttpMessageConverter<?>> partConverters = new LinkedList<>();
    partConverters.add(new ByteArrayHttpMessageConverter());
    StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
    stringHttpMessageConverter.setWriteAcceptCharset(false);
    partConverters.add(stringHttpMessageConverter);

    // Replace ResourceHttpMessageConverter with our implementation
    partConverters.add(new StreamingMessageConverter());

    // Converts from AllEncompassingFormHttpMessageConverter
    partConverters.add(new SourceHttpMessageConverter<>());
    partConverters.add(new MappingJackson2HttpMessageConverter());
    partConverters.add(new Jaxb2RootElementHttpMessageConverter());
//    partConverters.add(new MappingJackson2XmlHttpMessageConverter());
    return partConverters;
  }

  /**
   * A converter that prevents content-length being set in request/part header.
   */
  private static class StreamingMessageConverter extends ResourceHttpMessageConverter {
    @Override
    protected Long getContentLength(Resource resource, MediaType contentType) throws IOException {
      return null;
    }
  }
}
