package yangyd.fileserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import yangyd.anthology.web.NamedInputStreamResource;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringReader;

/**
 * This is the client side of streaming multipart upload.
 */
@Component
public class Uploader {
  @Autowired @Qualifier("multipart") RestTemplate restTemplate;

  @Value("http://localhost:9000/upload")
  private String uploadURL;

  public void doUploadSync(InputStream input, String fileName) {
    MultiValueMap<String, Object> payload = new LinkedMultiValueMap<>();
    payload.add("name 1", "value 1");
    payload.add("name 2", "value 2+1");
    payload.add("name 2", "value 2+2");

    payload.add("fileupload", new NamedInputStreamResource(input, fileName));

    Source xml = new StreamSource(new StringReader("<root><child/></root>"));
    payload.add("xml", xml);

    restTemplate.postForLocation(uploadURL, payload);
  }
}
