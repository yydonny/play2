package yangyd.fileserver.web;

import org.apache.tomcat.util.http.fileupload.*;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.util.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import yangyd.anthology.misc.Utils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A detailed example of handling the 'multipart/form-data' MIME type.
 */
@RestController
@RequestMapping("/upload")
class UploadController {
  private static final Logger logger = LoggerFactory.getLogger(UploadController.class);
  
  @Value("${anthology.web.uploadDir:}")
  private String uploadDirPath;
  private File uploadDir;
  
  @PostConstruct
  void init() {
    if (StringUtils.isEmpty(uploadDirPath)) {
      uploadDir = Utils.tempDir();
    } else {
      uploadDir = new File(uploadDirPath);
    }
    if (!uploadDir.isDirectory()) {
      throw new RuntimeException("Unable to find place to hold uploaded file: please specify `anthology.web.uploadDir` properly");
    }
  }

  // spring-boot's multipart support must be disabled in order to access multipart payload in our controller
  // set `spring.http.multipart.enabled=false` in the application configuration (`multipart.enabled` in Spring-boot 1.3 and before)

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<List<String>> upload(HttpServletRequest request) throws IOException, FileUploadException {

    Enumeration<String> headers = request.getHeaderNames();
    while (headers.hasMoreElements()) {
      String h = headers.nextElement();
      logger.info(h + ": " + request.getHeader(h));
    }

    if (!ServletFileUpload.isMultipartContent(request)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    List<String> savedFiles = new LinkedList<>();
    FileItemIterator iter = new ServletFileUpload().getItemIterator(request);
    while (iter.hasNext()) {
      FileItemStream item = iter.next();
      String fieldName = item.getFieldName();

      if (item.isFormField()) {
        try (InputStream in = item.openStream()) {
          logger.info("Read form field: {} = {}", fieldName, Streams.asString(in));
        }

      } else {
        String itemName = item.getName();
        FileItemHeaders itemHeaders = item.getHeaders();
        Iterator<String> hter = itemHeaders.getHeaderNames();
        logger.info("Found file field '{}', with file name '{}'", fieldName, itemName);
        while (hter.hasNext()) {
          String h = hter.next();
          logger.info("  " + h + ": " + itemHeaders.getHeader(h));
        }

        // Process the stream (write to disk)
        File file = new File(uploadDir, itemName);
        try (InputStream in = item.openStream();
             OutputStream out = new FileOutputStream(file)) {
          IOUtils.copy(in, out);
        }
        logger.info("Saved as {}", file.getAbsolutePath());
        savedFiles.add(file.getAbsolutePath());
      }
      
    } // end while FileItemIterator

    // return a list of saved files.
    return new ResponseEntity<>(savedFiles, HttpStatus.OK);
  }

}
