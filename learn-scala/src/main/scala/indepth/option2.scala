package indepth

import java.io.File
import java.sql.{Connection, DriverManager}
import javax.servlet.http.HttpSession

object option2 extends App {
  def getDirectory(path: Option[String]): File =
    path.map(n => new File(n)).filter(_.isDirectory).getOrElse(
      new File(System.getProperty("java.io.tmpdir")))

  def login(username: Option[String]) = for (n <- username) { println("login for: " + n) }

  def checkPassword(n: String, p: Array[Char]) = true
  def login2(session: Option[HttpSession], username: Option[String], password: Option[Array[Char]]) =
    for (n <- username; p <- password if checkPassword(n, p)) {
      session foreach (_.setAttribute("username", n))
    }

  def createConnection(conn_url: Option[String],
                       conn_user: Option[String],
                       conn_pw: Option[String]) : Option[Connection] =
    for {
      url <- conn_url
      user <- conn_user
      pw <- conn_pw
    } yield DriverManager.getConnection(url, user, pw)

  // note how type parameters are used to determine order of parameters
  def monad3[A,B,C,D](f: Function3[A, B, C, D]) =
    (oa: Option[A], ob: Option[B], oc: Option[C]) => for (a <- oa; b <- ob; c <- oc) yield f(a,b,c)

//  println(monad3(DriverManager.getConnection)(Some(null), Some(null), Some(null)))

    // !!! Some(null) is not None!!!
    println(monad3(DriverManager.getConnection)(None, None, None))
}
