package indepth

case class SimulationMessage()
case class SimulationContext()

trait SimulationEntity {
  // always give an empty implementation
  def handle(msg:SimulationMessage, ctx: SimulationContext) = {}
}

trait NetworkEntity extends SimulationEntity {
  // always use override keyword
  override def handle(msg:SimulationMessage, ctx: SimulationContext) = {
    msg match {
      case _ => super.handle(msg, ctx)
    }
  }
}





object emptyImpl extends App {

}
