package indepth

trait Prop {

  // Avoid abstract val in traits, use lazy val or constructor param instead
  val name: String
  override val toString = String.format("Prop(%s)", name) // a FIELD, not method !!!
}

object property extends App {

  // initialization of traits in the order of writing
  // when trait Prop gets initialized, the toString field uses the uninitialized name field
  println(new Prop { override val name = "jjj" }) // print 'Prop(null)'

  println(new { override val name = "jjj" } with Prop) // print 'Prop(jjj)'

}
