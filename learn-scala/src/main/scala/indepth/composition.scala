package indepth

// traits can't have constructor params
trait Logger {
  def log(msg:String) = println(msg)
}

trait RemoteLogger extends Logger {
  override def log(msg:String) = {}
}

trait NullLogger extends Logger {
  override def log(msg:String) = {}
}

trait WithLogger {
  val logger: Logger = new Logger {} // traits can't be initialized directly
}

trait WithRemoteLogger extends WithLogger {
  override val logger: RemoteLogger = new RemoteLogger {} // logger is not private, thus can be overridden
}

trait WithNullLogger extends WithLogger {
  override val logger: NullLogger = new NullLogger {}
}

trait DataAccess extends WithLogger {
  def query[A](in:String) = {
    logger.log(in)
    // ...
  }
}

object composition extends App {
  val aaa = new DataAccess with WithNullLogger // mixin new field member (not just methods) on the fly
}
