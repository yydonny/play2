package indepth

object implicit1append extends App {

  // View Bound: T must be converted to Ordered[T]
  def quickSort1[T <% Ordered[T]](list: List[T]): List[T] =
    list match {
      case pivotal::others =>
        val (left, right) = others partition {_ < pivotal}
        quickSort1(left) ++ (pivotal :: quickSort1(right))
      case _ => Nil
    }

  // Context bound [T: OrderedView]
  // declare requirement for an implicit "container object" for T (OrderedView[T])
  // that object is indeed a function converting T to Ordered[T]
  // because such an conversion (container object) exist from the Scala library as implicit,
  // it's silently available as if we write ==>(val view = implicitly[OrderedView[T]] )
  // so `<` is available because t become view(t)
  type OrderedView[T] = T => Ordered[T]
  def quickSort2[T: OrderedView](list: List[T]): List[T] =
    list match {
      case pivotal::others =>
        val (left, right) = others partition {_ < pivotal}
        quickSort2(left) ++ (pivotal :: quickSort2(right))
      case _ => Nil
    }

  // We actually have an OrderedView[T] in the Scala library: Ordering[T]
  // Ordering doesn't require extension as Ordered did, thus supports
  // defining different ordering for the same type
  def quickSort3[T](list:List[T])(implicit ord:Ordering[T]): List[T] = {
    import ord._
    list match {
      case pivotal::others =>
        val (left, right) = others partition {_ < pivotal}
        quickSort3(left) ++ (pivotal :: quickSort3(right))
      case _ => Nil
    }
  }

  // Same as quickSort3
  def quickSort4[T: Ordering](list: List[T]): List[T] = {
    val ord = implicitly[Ordering[T]]
    import ord._
    list match {
      case pivotal::others =>
        val (left, right) = others partition {_ < pivotal}
        quickSort2(left) ++ (pivotal :: quickSort4(right))
      case _ => Nil
    }
  }

  val list = List(2,8,3,12,8,434,6,77,3)
  println(quickSort1(list))
  println(quickSort2(list))
  println(quickSort3(list))
  println(quickSort4(list))
}
