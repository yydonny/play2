package indepth

object types1 extends App {
  Faa.bar(Faa.t_o) // Compiles fine (duck type, reflective call)

  val x = new Foo3 { type B = Traversable[Int] } // OK, List extends Traversable
//  val y = new Foo3 { type B = Set[Int] } // No good, List doesn't extend Set

  x.foo(Set(1))
//  y.foo(Set(2))
}

// Below is related to Covariance
// http://twitter.github.io/scala_school/type-basics.html
// see the twitter.* package

class Foo3 {
  // Type constraint (Lower bound)
  // The actual type must be a super type: List extends B
  type B >: List[Int]
  def foo(b: B): B = b

  // same as above
  def foo2[B >: List[Int]](b: B): B = b
}

class Bar3 {
  // Type constraint (Upper bound)
  // B must extend from Traversable
  // Often redundant because of Polymorphic (are you sure?)
  type B <: Traversable[Int]
  def count(b: B): Int = b.foldLeft(0)(_ + _)
}

object Faa {
  type Resource = {
    def close(): Unit
  }
  def dispose(r: Resource): Unit = r.close()

  def foo(): Unit = {
    dispose(System.in) // type coercion
  }
  def bar(t: Faa#T): Unit = println(t.x)

  object t_o { // can be coerced as Faa#T
    type X = Int
    def x: X = 6
    type Y = String
    def y: Y = "555"
  }
}

class Faa {
  type AbstractType
  type MyType1 = String
  type T = {
    type X = Int
    def x: X
    type Y
    def y: Y
  }
}
