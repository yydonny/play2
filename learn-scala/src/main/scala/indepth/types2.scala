package indepth

object types2 extends App {
  val x = new IntStore(5)
  val y = new IntStore(12)
  val handle = x.observe(println)
  x.set(2)
  x.unobserve(handle)
//  y.unobserve(handle) // type mismatch: x.Handle is not y.Handle
  x.set(4) // not printed
}

class IntStore(private var value: Int) extends Observable with DefaultHandles {
  def get = value
  def set(v: Int): Unit = {
    value = v
    notifyListeners()
  }
  override def toString = s"IntStore($value)"
}

trait Observable {
  // Abstract type (to be defined by subclasses)
  // Notice that this type is bounded specifically with the current object
  type Handle

  // this.type refers to the type of the current object, and changes with inheritance
  var callbacks: Map[Handle, this.type => Unit] = Map.empty

  def observe(fn: this.type => Unit): Handle = {
    val handle = createHandle(fn)
    callbacks += (handle -> fn)
    handle
  }

  def unobserve(handle: Handle): Unit = { callbacks -= handle }

  protected def createHandle(fn: this.type => Unit): Handle // to be implemented by subclasses

  protected def notifyListeners(): Unit = for (fn <- callbacks.values) fn(this)
}

trait DefaultHandles extends Observable {
  type Handle = (this.type => Unit) // implement the abstract type
  protected def createHandle(fn: this.type => Unit): Handle = fn
}
