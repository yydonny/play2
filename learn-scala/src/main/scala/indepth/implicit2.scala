package indepth

import java.io.File
import java.security.{AccessController, PrivilegedAction}

import yd.complex

trait BinaryFormat[T] {
  def asBinary(t: T): Array[Byte]
}

object holder {
  trait Foo
  object Foo {
    implicit val x = new Foo {
      override def toString = "Foo Companion"
    }

    // implicitly[List[Foo]], for parameterized implicit lookup
    implicit val lf = List(new Foo {})

    // enable Foo be viewed as "TYPE CLASS" BinaryFormat, even though Foo doesn't extend BinaryFormat[Foo]
    // So when code requires BinaryFormat sees a Foo, it knows how to find the corresponding BinaryFormat
    implicit lazy val bf = new BinaryFormat[Foo] {
      override def asBinary(t: Foo): Array[Byte] = "Serialized Foo".toCharArray map { _.toByte }
    }

    trait Bar
    // there's no companion object for Bar, but outer scope(Foo) are also searched
    // Singleton type?
    implicit def b = new Bar {
      override def toString = "Implicit Foo.Bar"
    }

    // implicit conversion, enables foo be used as bar
    implicit def foobar(foo:Foo): Bar = new Bar {}
  }
}

object MySecurityImplicits { // allows any ()=>T be used as a PrivilegedAction
  implicit def f2pa[T](fn: () => T): PrivilegedAction[T] =
    new PrivilegedAction[T] { override def run(): T = fn() }
}

// Make Java stuff fancy, while keeping backward compatibility
object FileWrapper {
  implicit def wrap(file: File): FileWrapper = new FileWrapper(file)
  implicit def unwrap(fw: FileWrapper): File = fw.file
}
class FileWrapper(val file: File) {
  def /(sub: String) = new FileWrapper(new File(file, sub))
  override def toString = file.getCanonicalPath
}

object implicit2 extends App {
  import holder.Foo
  println(implicitly[List[Foo]]) // implicit lookup with type parameter
  println(implicitly[BinaryFormat[Foo]]) // implicit lookup with type parameter
  println(implicitly[Foo.Bar])

  import MySecurityImplicits._
  AccessController.doPrivileged({() => println("12") })

  import FileWrapper._
  println(new File(".")/"test.txt")
  println((new File(".")/"test.txt").exists())


  // Advanced use of implicit view

  // Create an implicit view on Double that converts to a *new type* with a toComplex method.
  // This also allows double be used directly as ComplexNumber.
  implicit def r2c(x: Double) = new { def toComplex = complex.ComplexNumber(x, 0.0) }

  // However, this may be a better way to providing implicit conversion
  // since normally people would just import complex._
//  import complex.r2c

  println(1.0 + 5.0 * complex.i)
//  println(5.0 toComplex)
}
