package indepth

import java.util.concurrent.{Callable, Executors, ScheduledExecutorService, TimeUnit}

import scala.language.implicitConversions

object types3Foo {
  val executorService: ScheduledExecutorService = Executors.newScheduledThreadPool(2)

  // Implicit conversion to Java Runnable and Callable

  // Important things to notice:
  // 1. the difference between the two: for Runnable as run() is void method, f can be used without parenthesis.
  //    However for Callable, f must be called with parenthesis explicitly,
  //    so that the compiler knows that call() returns the return value of f, instead of f itself.
  //
  // 2. For Callable, the parameter type must be () => A instead of just => A (as in Runnable)
  //
  // 3. The declaration of type covariance (lower bound)

  implicit def callable[A <: Traversable[Int]](f: () ⇒ A): Callable[A] = new Callable[A] {override def call(): A = f()}

  implicit def runnable(f: ⇒ Unit): Runnable = new Runnable() { def run(): Unit = f }

  // and also in Scala 2.12, you no longer need to define these conversion yourself.

}

object types3 extends App {
  import types3Foo._
  val list: Traversable[Int] = executorService.schedule(() ⇒ Seq(1,2,3,4,5), 2, TimeUnit.SECONDS).get()
  for (x ← list) println(x)
  executorService.shutdown()
}
