package indepth

object implicit1 extends App {

  // Implicit lookup happens when you call a method which not supported by the object's class
  // String does not support the method map, but StringOps does,
  // and there's an implicit conversion from String to StringOps available
  println("abc".map(_.toInt))

  // Implicit parameter are passed to method calls like any other parameter,
  // but the compiler tries to fill them in automatically (Think about dependency injection)
  // scala.language.implicitConversions
  def foo[T](t: T)(implicit integral: Integral[T]) = { println(integral.abs(t)) }
  foo('f')
  // foo(12.93) // Error:(12, 6) could not find implicit value for parameter integral: Integral[Double]

  // some more examples
  implicit val imp_n: Int = 5
  def add5(x: Int)(implicit y: Int) = x + y
  println(add5(12)) // takes imp_n from the current scope

  import scala.collection.JavaConversions.mapAsScalaMap
  println(System.getenv()("USERPROFILE")) // implicitly conversion from java Map to scala Map

  // The conversion `option2Iterable` is defined by Option's companion object
  val xy1 = for {
    x <- List(1, 2, 3)
    y <- Some(x)
  } yield (x, y)
  val xy2 = List(1, 2, 3).flatMap(x => Some(x) map {y => (x, y)}) // same as above
  println(xy1)
  println(xy2)

  // View bounds
  // CQ can be any class, as long as there is an implicit conversion available from its type to Seq[T]
  // That's why we pass in a implicit parameter: `conv`
  // we don't even need to use conv directly in our code
  def getIndex[T, CQ](seq: CQ, value: T)(implicit conv: CQ => Seq[T]) = seq.indexOf(value)
  println(getIndex("abc", 'a'))

  // syntactic sugar of above (deprecated)
  def getIndex2[T, CQ <% Seq[T]](seq: CQ, value: T) = seq.indexOf(value)
  println(getIndex2("abc", 'a'))

  // Context Bound
  // [T:Integral] means looking for the implicit container(context) object
  // of type Integral[T]
  // And we can access that object using `implicitly` Predef function
  def sum[T : Integral](list: List[T]): T = {
    val integral = implicitly[Integral[T]]
    import integral._   // get the implicits in question into scope
    list.foldLeft(zero)(_ + _)
  }

  // View bound is just a special case of context bound
  // you can eliminate view bounds by making the conversion function type
  // serve as the container needed here
  type AsSeq[CQ] = CQ => Seq[_]
  def getIndex3[T, CQ : AsSeq](seq: CQ, value: T) = seq.indexOf(value)
  println(getIndex3("abc", 'c'))

  // To make it scary......
  def getIndex4[T, CQ : ({type AS[Q] = Q => Seq[_]})#AS](seq: CQ, value: T) = seq.indexOf(value)
  println(getIndex3("abc", 'c'))


  // def sorted[B >: A](implicit ord: Ordering[B])
  // `sorted` looks inside the object Ordering, companion to the class Ordering,
  // and finds an implicit Ordering[Int] there
  println(List(4, 2, 3).sorted)

  // from Ordering.scala...
  // trait IntOrdering extends Ordering[Int] { }
  // implicit object Int extends IntOrdering


  case class A(n: Int) {
    def +(other: A) = new A(n + other.n)
  }
  object A {
    implicit def fromInt(n: Int): A = new A(n)

    implicit val ord = new Ordering[A] {
      def compare(x: A, y: A) = implicitly[Ordering[Int]].compare(x.n, y.n) // manual lookup for specific implicit object
    }
  }
  // This becomes possible:
  println(1 + new A(1))
  // because it is converted into this:
  println(A.fromInt(1) + new A(1))

  // sorted expects an Ordering[A], which doesn't exist in the Ordering object
  // So we have to define ourselves (above), and
  // Scala looks for implicits in the 'implicit scope' of type arguments
  println(List(new A(5), new A(2)).sorted)
}
