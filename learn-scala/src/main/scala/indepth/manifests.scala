package indepth

import scala.reflect.ClassTag

object manifests extends App {

//  def first[T](x: Array[T]) = Array(x(0)) // compile error: no ClassTag available
  def first[T: ClassTag](x: Array[T]) = Array(x(0))

  println(first(Array(1,2,3)).getClass) // => class [I, i.e. int[]


}
