package yd

sealed trait Bounce[A]

case class Done[A](result: A) extends Bounce[A]

case class Call[A](thunk: () => Bounce[A]) extends Bounce[A]

/**
  * This example demonstrates using trampoline to save a recursion that's not @tailrec.
  * Based on the following discussion:
  *
  * @see http://stackoverflow.com/questions/2137619/scala-equivalent-to-python-generators
  *      http://blog.richdougherty.com/2009/04/tail-calls-tailrec-and-trampolines.html
  */
object trampoline {
  def trampoline[A](bounce: Bounce[A]): A = bounce match {
    case Call(thunk) => trampoline(thunk())
    case Done(x) => x
  }

}
