package yd.matrix

object MatrixService {
  def multiply(a: Matrix, b: Matrix)
              // implicit default
              (implicit ts: ThreadStrategy = SimpleStrategy): Matrix = {
    assert(a.colRank == b.rowRank)
    // create buffer matrix to hold the result
    val buf = new Array[Array[Double]](a.rowRank)
    for (i <- 0 until a.rowRank) {
      buf(i) = new Array[Double](b.colRank)
    }

    def compute(row: Int, col: Int): Unit = { // compute a single cell in the buf
      val pairs = a.row(row).zip(b.col(col))
      val products = for { (x, y) <- pairs } yield x * y
      buf(row)(col) = products.sum
    }

    val computation = for { // array of functions
      i <- 0 until a.rowRank
      j <- 0 until b.colRank
    } yield ts.init { () => compute(i, j) }

    computation foreach { _() }
    new Matrix(buf)
  }
}
