package yd.matrix

class Matrix (repr: Array[Array[Double]]) {
  lazy val rowRank = repr.length
  lazy val colRank = if (rowRank > 0) repr(0).length else 0
  def row(idx: Int): Seq[Double] = repr(idx).toList
  def col(idx: Int): Seq[Double] = {
    repr.foldRight(List.empty[Double]){ (row, buf) => row(idx) :: buf }
  }

  override def toString = repr.foldLeft("") { (buf, row) =>
    buf + row.mkString("\n|", " | ", "|")
  }
}
