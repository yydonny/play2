package yd.matrix

import java.util.concurrent.{Callable, Executors}

trait ThreadStrategy {
  def init[A](fn: () => A): () => A
}

object SimpleStrategy extends ThreadStrategy {
  override def init[A](fn: () => A): () => A = fn
}

object FTPStrategy extends ThreadStrategy {
  implicit def f2c[A](fn: ()=>A): Callable[A] = new Callable[A] { override def call(): A = fn() }
  val pool = Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors)
  override def init[A](fn: () => A): () => A = {
    val f = pool.submit(fn)
    () => f.get()
  }
}
