package yd.matrix

object main extends App {
  def foo(msg: String) = println(msg)
  implicit def i2s(x: Int): String = String.valueOf(x)

  foo(12)
  val ma = new Matrix(Array(
    Array(1,2,3),
    Array(4,5,6),
    Array(7,8,9)
  ))

  val mb = new Matrix(Array(
    Array(1),
    Array(1),
    Array(1)
  ))

  println(ma)
  println(ma.row(1))
  println(ma.col(1))

  // Strategy pattern
  println(MatrixService.multiply(ma, mb))
  println(MatrixService.multiply(ma, mb)(FTPStrategy))
}
