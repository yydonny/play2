package yd.typeclass

object statistics {
  // think type class as a better approach of the Adapter pattern.
  // it allows you to demand a set of capabilities for a given type in your methods,
  // but not necessarily have them defined by the interfaces of that type.

  // here we want to operate on the type T with 'NumberLike' capabilities,
  // while not require T to implement NumberLike directly

  import math.NumberLike

  // constrain T to types that are members of a specific type-class (NumberLike)
  def mean[T](xs: Vector[T])(implicit tc: NumberLike[T]): T =
    tc.divide(xs.reduce(tc.plus), xs.size)

  def median[T](xs: Vector[T]): T = xs(xs.size / 2)

  def quartiles[T](xs: Vector[T]): (T, T, T) =
    (xs(xs.size / 4), median(xs), xs(xs.size / 4 * 3))

  // type-class in the form of context bound
  def iqr[T: NumberLike](xs: Vector[T]): T = quartiles(xs) match {
    case (lowerQuartile, _, upperQuartile) ⇒
      val tc = implicitly[NumberLike[T]] // however, this only works when the type-class has only one type parameter
      tc.minus(upperQuartile, lowerQuartile)
  }

  //  def median(xs: Vector[Double]): Double = xs(xs.size / 2)
  //
  //  def quartiles(xs: Vector[Double]): (Double, Double, Double) =
  //    (xs(xs.size / 4), median(xs), xs(xs.size / 4 * 3))
  //
  //  def iqr(xs: Vector[Double]): Double = quartiles(xs) match {
  //    case (lowerQuartile, _, upperQuartile) => upperQuartile - lowerQuartile
  //  }
}
