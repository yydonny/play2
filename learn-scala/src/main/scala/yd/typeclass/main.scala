package yd.typeclass

object main extends App {
  val numbers = Vector[Double](13, 23.0, 42, 45, 61, 7.3, 96, 100, 199, 420, 900, 383.9)
  println(statistics.mean(numbers))
  println(statistics.iqr(numbers))
}
