package yd.typeclass

import scala.annotation.implicitNotFound

object math {
  // A type class is defined as a parametrized trait: NumberLike[T]
  // You can have a specific type 'join' the type-class by 1) implementing it with your type as parameter
  // and 2) make the implementation available in the implicit scope

  //Display this message when implicit lookup failed to find an implementation of this trait
  @implicitNotFound("No member of type class NumberLike in scope for ${T}")
  trait NumberLike[T] {
    def plus(x: T, y: T): T
    def divide(x: T, y: Int): T
    def minus(x: T, y: T): T
  }

  object NumberLike {
    // here Double and Int are made member of NumberLike
    // putting them in the companion object makes them automatically available to user of the type-class (by implicit resolution)

    implicit object NumberLikeDouble extends NumberLike[Double] {
      def plus(x: Double, y: Double): Double = x + y
      def divide(x: Double, y: Int): Double = x / y
      def minus(x: Double, y: Double): Double = x - y
    }

    implicit object NumberLikeInt extends NumberLike[Int] {
      def plus(x: Int, y: Int): Int = x + y
      def divide(x: Int, y: Int): Int = x / y
      def minus(x: Int, y: Int): Int = x - y
    }
  }

  // A very common use case in third-party libraries is that of object serialization and deserialization,
  // most notably to and from JSON. By making your classes members of an appropriate formatter type class,
  // you can customize the way your classes are serialized to JSON.

  // Mapping between Scala types and ones supported by your database driver is also commonly made
  // customizable and extensible via type classes.
}
