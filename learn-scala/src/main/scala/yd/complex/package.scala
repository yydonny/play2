package yd

package object complex {
  val i = ComplexNumber(0.0, 1.0)
  implicit def r2c(r: Double): ComplexNumber = ComplexNumber(r, 0.0)
}
