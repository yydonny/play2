package yd.complex

case class ComplexNumber(re: Double, im: Double) {
  def *(z: ComplexNumber) = ComplexNumber(
    re * z.re + im * z.im,
    re * z.im + im * z.re)

  def +(z: ComplexNumber) = ComplexNumber(re + z.re, im + z.im)
}
