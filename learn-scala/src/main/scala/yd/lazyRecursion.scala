package yd

import scala.collection.mutable.ArrayBuffer

/**
  * A demonstration of recursively building stream/iterator that's similar to python generators.
  * Examples here are based on the discussion in this StackOverflow thread:
  *
  * @see http://stackoverflow.com/questions/2137619/scala-equivalent-to-python-generators
  *
  */
object lazyRecursion extends App {
  import scala.language.implicitConversions

  private val cab = classOf[ArrayBuffer[_]]
  for (c ← classStream1(cab).slice(0, 4)) { println(c) }
  println("--------------------")
  for (c ← classStream2(cab)().slice(0, 4)) { println(c) }
  println("--------------------")
  for (c ← classIterator2(cab).slice(0, 4)) { println(c) }

  /**
    * The plain method that print class inheritance graph
    */
  def printClass(clazz:Class[_], indent:String=""): Unit = clazz match {
      case null ⇒
      case _ ⇒
        println(indent + clazz)
        printClass(clazz.getSuperclass, indent + "  ")
        for (c <- clazz.getInterfaces) {
          printClass(c, indent + "  ")
        }
    }

  /**
    * Generate a stream that emits each class in the inheritance graph upward.
    * Non-tail recursive isn't a problem since Stream is lazily evaluated.
    *
    * @param clazz the start point
    * @return the stream
    */
  def classStream1(clazz: Class[_]): Stream[Class[_]] = clazz match {
    case null ⇒ Stream.empty
    case _ ⇒ clazz #::
        classStream1(clazz.getSuperclass) #:::
        {println("calling getInterfaces()");clazz.getInterfaces.toStream.flatMap(classStream1)} #:::
        Stream.empty
      // a word on recursive:
      // the first call creates a stream concatenated by four, and the latter stream will not be initialized (thus triggers recursion)
      // before streams before it are exhausted. We can see that getInterfaces() is never called if we only consume first few elements.
  }

  /**
    * Generate a stream that emits every class in the inheritance graph upward.
    * This differ with classStream1 in that it return a "supplier of stream".
    *
    * Scala stream always eagerly compute the first element on construction, so to achieve full laziness,
    * this method return a "supplier of stream" instead of a stream
    *
    * @param clazz
    * @return
    */
  def classStream2(clazz: Class[_]): () ⇒ Stream[Class[_]] = () ⇒ clazz match {
    case null ⇒ Stream.empty
    case _ ⇒ clazz #::
        classStream2(clazz.getSuperclass)() #:::
        clazz.getInterfaces.toStream.flatMap(classStream2(_)()) #:::
        Stream.empty
  }

  /**
    * The equivalent of classStream1 using Iterator
    * @param clazz
    * @return
    */
  def classIterator1(clazz: Class[_]): Iterator[Class[_]] = clazz match {
    case null ⇒ Iterator.empty
    case _ ⇒ Iterator(clazz) ++
        classIterator1(clazz.getSuperclass) ++
        clazz.getInterfaces.iterator.flatMap(classIterator1)
  }

  /**
    * The equivalent of classStream2 using Iterator
    * Unlike Stream, Iterator can be extended, so we can achieve a cleaner API
    * through a special iterator implementation.
    * @param clazz
    * @return
    */
  def classIterator2(clazz: Class[_]): () ⇒ Iterator[Class[_]] = () ⇒ clazz match {
    case null ⇒ Iterator.empty
    case _ ⇒ Iterator(clazz) ++
        classIterator2(clazz.getSuperclass) ++
        clazz.getInterfaces.iterator.flatMap(classIterator2)
  }
  implicit def ccc[A](is: () ⇒ Iterator[A]): Iterator[A] = new LazyIterator[A](is)
}

class LazyIterator[A](iterSupplier: () ⇒ Iterator[A]) extends Iterator[A] {
  lazy val iter = iterSupplier()
  override def hasNext: Boolean = iter.hasNext
  override def next(): A = iter.next()
}
