package fpinscala

object ch2 extends App {
  def partial1[A, B, C](f: (A, B) => C, a: A): B => C =
    (b: B) => f(a, b)

  def curry1[A, B, C](f: (A, B) => C): A => (B => C) =
    (a: A) => partial1(f, a)

  // A => B => C is same as A => (B => C)
  def uncurry1[A, B, C](cf: A => B => C): (A, B) => C =
    (a: A, b: B) => cf(a)(b)

  def compose1[A, B, C](f: A => B, g: B => C): A => C =
    (a: A) => g(f(a))


  val f = (a: Int, b: Int) => a + b
  val g = partial1(f, 4)

  println(partial1(f, 4)(6))
  println(curry1(f)(5)(8))
  println(uncurry1(curry1(f))(5, 8))
  compose1(g, println)(6) // impure!
}
