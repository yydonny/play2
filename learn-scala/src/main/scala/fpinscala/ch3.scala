package fpinscala

import scala.annotation.tailrec

sealed trait ListI[+A]
case object NilI extends ListI[Nothing]
case class Cons[+A](head: A, tails: ListI[A]) extends ListI[A]

object ListI { // companion object of ListI[+A]
  def apply[A](as: A*): ListI[A] = // varargs
    if (as.isEmpty) NilI else Cons(as.head, apply(as.tail: _*)) // destructive invocation

  @deprecated
  def foldRight[A, B](xs: ListI[A], prev: B)(fn: (A, B) => B): B = xs match {
    case NilI => prev
    case Cons(h, ts) => fn(h, foldRight(ts, prev)(fn))
  }

  // Note that foldLeft is tail-recursive, but foldRight isn't
  @tailrec
  def foldLeft[A, B](xs: ListI[A], prev: B)(fn: (B, A) => B): B = xs match {
    case NilI => prev
    case Cons(h, ts) => foldLeft(ts, fn(prev, h))(fn)
  }

  // The implementation of `foldRight` in terms of `reverse` and `foldLeft`
  // is a common trick for avoiding stack overflows
  @deprecated
  def foldRight2[A, B](xs: ListI[A], prev: B)(fn: (A, B) => B): B =
    foldLeft(reverse(xs), prev){(b, a) => fn(a, b)}

  // What the hell...
  def foldRight3[A, B](xs: ListI[A], prev: B)(fn: (A, B) => B): B =
    foldLeft(xs, (b:B) => b){(g, a) => b => g(fn(a, b))}(prev)

  def foldRight3a[A, B](xs: ListI[A], base: B)(fn: (A, B) => B): B = {
    type IDB = B => B
    val id: IDB = (b: B) => b
    val f2 = (g: IDB, a: A) => { (b: B) => g(fn(a, b)) }
    val f3 = foldLeft(xs, id)(f2)
    f3(base) // <--- f2(f2(f2(f2(f2(..., a[x]), a[x-1]), a[x-2]), ...), a[1]), a[0])
  }

  def sum(ints: ListI[Int]): Int = foldRight3(ints, 0)(_ + _)

  def length[A](xs: ListI[A]): Int = foldRight3(xs, 0) { (x, len) => len + 1 }

  def append[A](xs: ListI[A], a: A): ListI[A] = foldRight3(xs, Cons(a, NilI)) { (x, prev) => Cons(x, prev) }

  def reverse[A](xs: ListI[A]): ListI[A] =
    foldLeft(xs, NilI:ListI[A]) { (prev, x) => Cons(x, prev) } // specify NilI as ListI[A], not NilI.type

  def concat[A](xs: ListI[A], ys: ListI[A]): ListI[A] =
    foldRight3(xs, ys) { (a, prev) => Cons(a, prev) }

  def flatten[A](xss: ListI[ListI[A]]): ListI[A] =
    foldRight3(xss, NilI:ListI[A]) { (xs, prev) => concat(xs, prev) }

  def map[A, B](xs: ListI[A])(fn: A => B): ListI[B] =
    foldRight3(xs, NilI:ListI[B]) { (a, prev) => Cons(fn(a), prev) }

  def flatMap[A, B](xs: ListI[A])(fn: A => ListI[B]): ListI[B] =
    foldRight3(xs, NilI:ListI[B]) { (a, prev) => concat(fn(a), prev) }

  def filter[A](xs: ListI[A])(fn: A => Boolean): ListI[A] = ???



  def product(ds: ListI[Double]): Double = ds match {
    case NilI => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def tail[A](li: ListI[A]): ListI[A] = li match {
    case NilI => NilI
    case Cons(h, t) => t
  }

  def setHead[A](h: A, li: ListI[A]): ListI[A] = li match {
    case NilI => NilI
    case Cons(_, t) => Cons(h, t)
  }

  def drop[A](li: ListI[A], n: Int): ListI[A] = li match {
    case NilI => NilI
    case Cons(_, t) if n > 0 => drop(t, n - 1)
    case _ => li
  }

  // define dropWhile as curried function to help type inference when using this method
  // see below for usage
//  def dropWhile[A](li: ListI[A], f: A => Boolean): ListI[A] = li match {
  def dropWhile[A](li: ListI[A])(f: A => Boolean): ListI[A] = li match {
    case NilI => NilI
    case Cons(h, t) if f(h) => dropWhile(t)(f)
    case _ => li
  }

  /**
    * List(1,2,3,4) -> List(1,2,3)
    * @param li
    * @tparam A
    * @return
    */
  def pre[A](li: ListI[A]): ListI[A] = li match {
    case NilI => NilI
    case Cons(h, t) => t match {
      case NilI => NilI // remove the last element (pre(Cons(x, NilI)) -> NilI
      case _ => Cons(h, pre(t))
    }
  }
}

object ch3 extends App {
  val v = ListI(1,2,3,4,5) match { // fancy pattern match
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case _ => 42
  }
  println(v)
  println(ListI.drop(ListI(1,2,3,4,5), 3))
  println(ListI.pre(ListI(1,2,3,4,5)))
  println(ListI.dropWhile(ListI(1,2,3,4,5)){_ < 4})

  // reconstruct the list
  println(ListI.foldRight(ListI(1,2,3,4,5), NilI:ListI[Int])(Cons(_, _)))
  println(ListI.reverse(ListI(1,2,3,4,5)))

}
