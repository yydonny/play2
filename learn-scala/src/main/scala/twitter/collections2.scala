package twitter

// Examples of Collection utilities.

object collections2 extends App {

  val list1 = List(1,2,3)
  val list2 = 1 :: 2 :: 3 :: Nil

  // Seq is actually a traits, and implemented by List.
  // And Seq() creates List
  val seq1 = Seq(1,1,2,3)

  val tr1: Traversable[Int] = 1 to 10
  println(tr1 map { _ * 2 })
  println(tr1 find { _ % 2 == 0 }) // => Some(2)
  println(tr1 partition { _ % 2 == 0 }) // => (VectorEven, VectorOdd)
  println(tr1 groupBy { _ % 3 }) // classify numbers by (n mod 3), a map keyed with 0,1,2

  println(Set(1,1,2,3) + 12 + 2) // Set(1,2,3,12)
}
