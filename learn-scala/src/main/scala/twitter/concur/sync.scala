package twitter.concur

import java.util.concurrent.atomic.AtomicReference

class Person1(var name:String) {
  def setName(n:String): Unit = {
    this.synchronized { name = n } // have to synchronize to prevent shadow write
  }
}

class Person2(@volatile var name:String) {
  def setName(n:String): Unit = {
    name = n // volatile provide same level protection since String is immutable
  }
}

class Person3(val name:AtomicReference[String]) {
  def setName(n:String): Unit = {
    name.set(n)
  }
}

object sync extends App {


}
