package twitter.concur

import java.net.{Socket, ServerSocket}
import java.util.concurrent.Executors

class NetworkService1(port: Int, poolSize: Int) extends Runnable {
  val ss = new ServerSocket(port)
  val tp = Executors.newFixedThreadPool(poolSize)

  override def run(): Unit = {
//    while (true) {
//      // note that accept() blocks the thread
//      new Thread(new Handler(ss.accept())).start() // Naive way
//    }
    try {
      while (true) {
        // note that accept() blocks the thread
        tp.execute(new Handler(ss.accept())) // use thread pool
      }
    } finally {
      tp.shutdown()
    }
  }
}

class Handler(socket: Socket) extends Runnable {
  def message = (Thread.currentThread.getName + "\n").getBytes
  override def run(): Unit = {
    socket.getOutputStream.write(message)
    socket.getOutputStream.close()
  }
}
