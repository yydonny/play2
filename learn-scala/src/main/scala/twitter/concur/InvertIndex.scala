package twitter.concur

import java.util.concurrent._

import scala.collection.mutable
import scala.io.Source

object Main extends App {
  val queue = new LinkedBlockingQueue[String]()
  val poolSize = 7
  val pool = Executors.newFixedThreadPool(poolSize)
  for (i <- 1 to 7) {
    pool.submit(new Indexer(queue))
  }
  new Thread(new RecordReader("User.txt", queue)).start()
}

case class User(name: String, id: Int)

class InvertIndex1(users: mutable.Map[String, User]) {
  def this() = this(new mutable.HashMap[String, User] with mutable.SynchronizedMap[String, User])

  def add(user: User): Unit = {
    tokenize(user.name) foreach { users += _ -> user }
  }

  def tokenize(name: String) = name split " " map { _.toLowerCase }
}

import scala.collection.JavaConversions._
class InvertIndex2(users: ConcurrentMap[String, User])
    extends InvertIndex1(users) { // java's ConcurrentMap implicitly converted to mutable.Map
  def this() = this(new ConcurrentHashMap[String, User] )
}

trait UserMaker {
  val index: InvertIndex1 = new InvertIndex2
  def makeUser(line:String): User = line split " " match {
    case Array(name, userid) => User(name, userid.trim.toInt)
  }
}

class RecordReader(path: String, queue: BlockingQueue[String]) extends Runnable {
  override def run(): Unit = {
    Source.fromFile(path, "UTF-8").getLines foreach { queue.put(_) }
  }
}

abstract class Consumer[T](queue: BlockingQueue[T]) extends Runnable {
  def consume (x: T)

  override def run(): Unit = {
    while (true) { consume(queue.take) }
  }
}

class Indexer(q: BlockingQueue[String]) extends Consumer[String](q) with UserMaker {
  override def consume(x: String): Unit = {
    index.add(makeUser(x))
  }
}
