import scala.util.Sorting

// View Bounded: A must be able to implicitly convert to Int
class C1[A <% Int] {
  def add(x:A) = 33 + x
}

case class Person(name: String, age: Int)

object AgeOrdering extends Ordering[Person] {
  override def compare(a: Person, b: Person) = a.age compare b.age
}

trait MySeq[+A] {
  // A must be able to be viewed as B, and need the implicit Ordering on B
  def min[B >: A](implicit cmp: Ordering[B]): A = ???
}

object types2 extends App {
  // define our implicit conversion from string to int
  implicit def str2int(x: String) = x.toInt

  println(math.max("123", 12))

  // then we can use the view class..
  println(new C1[String]().add("12"))

  //  new Container[Float]() //Error No implicit view available from Float => Int.

  // Syntactic sugar for view bound

  //  def f1[A <% Int](a: A) = 33 + a
  def f1[A](a: A)(implicit ev: A => Int) = 33 + a

  println(f1("333"))

  println(implicitly[Ordering[Int]])

  // Context bound
  // for type A, an implicit (companion object of) type class Ordering[A] must exist
  def f21[A](a: A, b: A)(implicit ord: Ordering[A]) = {
    if (ord.lt(a, b)) a else b

    // or as following:
    //    import ord._
    //    if (a < b) a else b
  }

  // syntactic sugar of about
  def f22[A: Ordering](a: A, b: A) = if (implicitly[Ordering[A]].lt(a, b)) a else b

  // Examples from scala library: Ordering.scala
  val people = Array(
    Person("bob", 30),
    Person("ann", 32),
    Person("carl", 19))

  //  Sorting.quickSort(people) // Error:(46, 20) No implicit Ordering defined for Person.
  Sorting.quickSort(people)(AgeOrdering) // ok
  people foreach { println(_) }

  // implicit lookup can't find AgeOrdering (?), have to give it explicitly
  println(f21(Person("aaa", 12), Person("bbb", 15))(AgeOrdering))

  val a = Seq(1,2,3,4)
  println(a.min)
}
