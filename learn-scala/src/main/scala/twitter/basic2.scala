
// Functions are Objects
object addOne extends Function1[Int, Int] {
  def apply(m: Int) = m + 1
}

// A nice short-hand for extends Function1[Int, Int] is extends (Int => Int)
// Can not be named 'AddOne' as the name of its companion object conflicts with 'addOne'
class AddOne2 extends (Int => Int) {
  def apply(m: Int): Int = m + 1
}

object basic2 extends App {
  println(addOne(12))

  println(new AddOne2()(12))

  // Matching with guards
  val times = 1
  times match {
    case i if i == 1 => "one"
    case i if i == 2 => "two"
    case _ => "some other number"
  }

  // Matching on type
  def bigger(o: Any): Any = {
    o match {
      case i: Int if i < 0 => i - 1
      case i: Int => i + 1
      case d: Double if d < 0.0 => d - 0.1
      case d: Double => d + 0.1
      case text: String => text + "s"
    }
  }

  // Matching on class members
  case class Calculator(brand: String, model: String)
  val hp20b = Calculator("hp", "20B")
  val hp30b = Calculator("hp", "30B")

  def calcType(calc: Calculator) = calc match {
    case Calculator("hp", "20B") => "financial"
    case Calculator("hp", "48G") => "scientific"
    case Calculator("hp", "30B") => "business"
//    case Calculator(ourBrand, ourModel) => "Calculator: %s %s is of unknown type".format(ourBrand, ourModel)
    case c@Calculator(_, _) => "Calculator: %s of unknown type".format(c) // diao bao le
  }


}
