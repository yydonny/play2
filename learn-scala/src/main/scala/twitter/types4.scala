package twitter

trait Foo {
  type A
  val x: A
  def getX: A = x
}

class Maker[A](implicit  manifest: Manifest[A]) {
  def make: A = manifest.runtimeClass.newInstance.asInstanceOf[A]
}

object types4 extends App {

  // Abstract Type Member
  println(new Foo {
    override type A = Int
    override val x: A = 12
  }.getX)

  println(new Foo {
    override type A = String
    override val x: A = "12"
  }.getX)

  // Access type information (erased normally)
  println(new Maker[String].make.length) // 0 (empty string created by ...newInstance)

  // Duck Type
  // uses reflection, has performance impact
  def foo(x: {def get: Int}) = 12 + x.get
  println(foo((new OptionWrapper).wrap(12))) // 24

  // we can't use types3.optionWrapper here, because types3 extends App which extends DelayedInit,
  // and fields defined there only exist when their `main` gets called (which will not happen)

}
