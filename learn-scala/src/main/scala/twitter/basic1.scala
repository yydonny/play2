import java.net.{Socket, SocketAddress}

class Calculator(val brand:String, val model:String) {
  val color = brand match {
    case "TI" => "blue"
    case "HP" => "black"
    case _ => "white"
  }

  def add (m:Int, n:Int) = m + n
}

trait Car { val brand: String }
trait Shiny { val shineReflection: Int }
class BMW extends Car with Shiny {
  val brand = "BMW"
  val shineReflection = 12
}

trait Cache[K, V] {
  def get(key:K): V
  def put(key:K, value:V)
  def delete(key:K)
}

object basic1 extends App {

  val calc = new Calculator("HP", "x22");
  println(calc.add(12, 23))

  type makeSocket = SocketAddress => Socket

  val addr2Inet: SocketAddress => Long = { case x => 42L } // anonymous function, implicit pattern matching
  val inet2Socket: Long => Socket = { case x => null }
  val factory: makeSocket = addr2Inet andThen inet2Socket

  println("done")
}
