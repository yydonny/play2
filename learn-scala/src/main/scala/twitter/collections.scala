import java.net.InetSocketAddress

object collections extends App {
  val entry = "http" -> 8080 // syntactic sugar: tuple2
  val (key, value) = entry // destructive assignation

  def x2(x:Int) = 2 * x

  val map1 = Map("one" -> 1, "two" -> 2)
  val map2 = Map("timesTwo" -> { x2(_) }) // anonymous function as value

  println(key, value)

  def check(x: Option[Int]) = x match {
    case Some(n) => println(n + 2)
    case _ => println("none")
  }

  check(map1.get("two"))
  check(map1.get("three"))

  map1.get("three") foreach { println(_) } // safe operation against the Optioned value

  val squared: (Int => Int) = { Math.pow(_, 2).toInt }

  println(List(1,2,3) map squared reduce { _ + _ }) // = 1 + 4 + 9

  println(List(1, 2, 3) zip List("a", "b", "c")) // List((1,a), (2,b), (3,c))

  println((1 to 12 toList) partition {_ % 2 == 0}) // (List(even number),List(odd number))

  println(List(1,3,4,5,6).dropWhile(_ % 2 != 0)) // remove first elementS match the prediction (1 and 3 here)

  println(List(List(1, 2), List(3, 4)).flatten)


  // flatmap: map each element to a list-like, then flatten nested list-like-s
  val xs = Map("a" -> List(1,2,3), "b" -> List(4,5,6)).flatMap(_._2)
  println(xs) // List(1, 2, 3, 4, 5, 6)

  // super useful for nested iteration
  val chars = 'a' to 'z'

  // The outer flatmap: produce listS like [ba, bc, ..., bz] and then flatten them
  // Inner flatmap: produce list [ab] or [].
  // using flatmap instead of map because empty list can be automatically filtered out
  val perms = chars flatMap {
    a => chars flatMap {b => if (a != b) Seq("%c%c".format(a, b)) else Seq() }
  }

  println(chars.length)
  println(perms.length) // = 26 * 25, full permutation

  // The above is equivalent to the more concise for-comprehension...
  val perms2 = for {
    a <- chars
    b <- chars
    if a != b
  } yield "%c%c".format(a, b)


  val host: Option[String] = None
  val port: Option[Int] = None

  // more practical when used on Options
  // note we use map inner here
  // !!! for Option, null value is ignored by flatmap and map !!!
  val addr1: Option[InetSocketAddress] = host flatMap {
    h => port map { new InetSocketAddress(h, _) } // never reach here for null values
  }

  // yield also produce an Option, and takes care of null values automatically
  val addr2: Option[InetSocketAddress] = for {
    h <- host
    p <- port
  } yield new InetSocketAddress(h, p)

  println(addr2)

  // another example
  val xy1 = for {
    x <- List(1, null, 3)
    y <- Some(x)
  } yield (x, y)
  val xy2 = List(1, null, 3).flatMap(x => Some(x) map { (x, _) }) // same as above
  println(xy1) // List((1,1), (null,null), (3,3))  !!! Some(null) is not None !!!
  println(xy2)


  // define your own map with fold/foldRight
  def ydmap(l: List[Int], fn: Int => Int) =
    l.foldRight(List[Int]()) { (x: Int, xs: List[Int]) => fn(x) :: xs } // use curly brackets for lambda makes it a bit more readable

  // iteration over map: you get tuples
  val roster = Map("steve" -> 100, "bob" -> 101, "joe" -> 201)
  println(roster filter { case (k, v) => v < 200 }) // Map(steve -> 100, bob -> 101)
}
