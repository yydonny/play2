package twitter

class Covariant[+A] // java equivalent: ? extends A (`+` means upper bound)
class Contravariant[-A] // java equivalent: ? super A (useful when declaring parameters for function type)

trait Function1[-T1, +R] // as defined in the Scala library, (T1) ? R

class Animal { val sound = "rustle" }
class Bird extends Animal { override val sound = "call" }
class Chicken extends Bird { override val sound = "cluck" }

object types1 extends App {

  // generics and type inference
  val list1 = 2 :: 1 :: "bar" :: "forkjoin" :: Nil // List[Any]
  def drop1[A](l: List[A]) = l.tail

  // In scala all type inference is local.
  // That's why you have to specify type for function parameters
  def id[T](x: T) = x

  println(drop1(list1))

  // Type Variance
  val cv1: Covariant[AnyRef] = new Covariant[String] // +A: I need a A, I have a subclass of A
  val cv2: Contravariant[String] = new Contravariant[AnyRef] // -A: declare concrete, use abstract

  // Usually we want parameters of a abstract function to be able to generalize..

  // def foo[A, B](f: A => List[A], b: B) = f(b)   // <--- type mismatch

  // Function1[-T1, +R], declared as (Bird)=>String, but accept (Animal)=>String
  val getTweet: (Bird => String) = { (b:Animal) => b.sound }

  // Actually we want predicate as (-T => Boolean), but don't need to specify it here
  // This is because Scala defines type variance at declaration time of the trait (Function1 here).
  // T => Boolean is parsed to Function1[-T, +Boolean]
  def filter[T](list: Iterable[T], predicate: T => Boolean): Iterable[T] = ???


  // Bounded Type ( try compare this with covariance )
  // Usually used in method or custom type definition

  // def cacophony[T](things: Seq[T]) = things map (_.sound) // not compile
  // Allows subtype of Animal
  def biophony[T <: Animal](things: Seq[T]) = things map (_.sound)

  println(biophony(Seq(new Chicken, new Bird)))


  // Smart Covariance

  val flock = List(new Bird, new Bird)
  // calling -->> ::(x: A): List[A]
  println(new Chicken :: flock)

  // Generalizing bound
  // calling -->> ::[B >: A](x: B): List[B]
  // which allows generalization of types, thus accommodates elements of different types
  // List[Bird] -> List[Animal] -> List[AnyRef] -> List[Any]
  println(12 :: "43" :: new Animal :: flock)

  // Sometimes you do not care to be able to name a type variable...
  def count(l: List[_]) = l.size
  // and this is shorthand for:
  def count1(l: List[T forSome { type T }]) = l.size
  // although handy, this may lose type information...
  def drop2(l: List[_]) = l.tail // become List[Any]

  // to make it more complex...
  def hashCodes(l: Seq[_ <: AnyRef]) = l map (_.hashCode)
  //  hashCodes(Seq(1,2,3)) // type mismatch

}
