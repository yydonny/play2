import java.io.IOException
import java.sql.SQLException
import java.util.logging.Level

object functions extends App {

  def f(s: String) = "f(" + s + ")"
  def g(s: String) = "g(" + s + ")"

  def fg = f _ compose g _
  def gf = f _ andThen g _

  println(fg("x")) // f(g(x))
  println(gf("x")) // g(f(x))

  // Case statement is actually Partial Function
  val pf: PartialFunction[Int, String] = {
    case i if i % 2 == 0 => "even"
  }
  // this is how case statements are composed...
  val tf: (Int => String) = pf orElse { case _ => "odd" }

  println(tf(1)) // odd
  println(tf(2)) // even

  // Usage of Partial Functions
  //  in situations that might otherwise call for returning an Option
  type Classifier = PartialFunction[Throwable, Level]
  val cf1: Classifier = { case _:IOException => Level.WARNING } // '_:' can be omitted if used in a match block
  val cf2: Classifier = { case _:SQLException => Level.SEVERE }
  val cf: Classifier = cf1 orElse cf2 orElse { case _ => Level.FINEST } // must declare this type


  // another example for pattern matching (demystified)
  val one: PartialFunction[Int, String] = { case 1 => "one" }
  val two: PartialFunction[Int, String] = { case 2 => "two" }
  val three: PartialFunction[Int, String] = { case 3 => "three" }
  val wildcard: PartialFunction[Int, String] = { case _ => "something else" }
  val partial = one orElse two orElse three orElse wildcard

  // PartialFunction subclasses FunctionX, that's why we can do this
  println(List(1,2,3,4,5) filter { case i => i % 2 == 0 })
}
