package twitter

// M is parametrized type
trait Wrapper[M[_]] {
  def wrap[A](x: A): M[A]
  def unwrap[A](m: M[A]): A
}

class OptionWrapper extends Wrapper[Option] {
  def wrap[A](x: A): Option[A] = Some(x)
  def unwrap[A](m: Option[A]): A = m.get
}

object types3 extends App {
  val optionWrapper = new OptionWrapper

  println(optionWrapper.wrap(12))
  println(optionWrapper.unwrap(Some(12)))

  implicit val listWrapper = new Wrapper[List] {
    def wrap[A](x: A) = List(x)
    def unwrap[A](m: List[A]) = m.head
  }

  implicit val someWrapper = new Wrapper[Some] {
    def wrap[A](x: A): Some[A] = Some(x)
    def unwrap[A](m: Some[A]): A = m.get
  }

  def tuplize[M[_]: Wrapper, A, B](left: M[A], right: M[B]) = {
    val c = implicitly[Wrapper[M]]
    c.wrap(c.unwrap(left), c.unwrap(right))
  }

  println(tuplize(List(1), List(2))) // List((1,2))

  // No covariance here: have to use someWrapper, `optionWrapper` will not work.
  println(tuplize(Some(1), Some(2))) // Some((1,2))

}
