package bite

object optionAndSome extends App {

  // An Option[A] can only be an None, or a Some[A].
  val nameOptionNone: Option[String] = None
  val nameOptionSome: Option[String] = Some("aaaaaaaaa")
  nameOptionNone match {
    case Some(name) =>
      println(name.trim.toUpperCase)
    case None =>
      println("No name value")
  }

  // the constructor Some(*) is same as Option(*)
  Some(1) == Option(1) // true

  // the <- operator in `for` automatically unbox the Option
  val upper = for {
  //    name <- nameOptionNone
    name <- nameOptionSome
    trimmed <- Some(name.trim)
    upper <- Option(trimmed.toUpperCase) if trimmed.length != 0
  } yield upper
  println(upper getOrElse "---")

  // The most idiomatic way to use an Option is to treat it as a collection
  println(nameOptionNone map { _.trim } filter { _.length != 0 } map { _.toUpperCase } getOrElse "---")

}
