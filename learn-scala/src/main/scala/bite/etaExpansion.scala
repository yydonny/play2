package bite

/*
  method vs function, a.k.a. ETA Expansion

	- Functions are values just like integers, strings or other objects.
	- Methods on the other hand are not values; they don’t have a type and cannot exist on their own (they are an attribute of a structure in which they are defined).
	- Methods must be defined using the def keyword, while functions can be defined like any other value — by using def, val or lazy val, in which case the keyword determines whether the value is evaluated every time, only once or only once but at the point of usage.
	- Methods with no parenthesis is special and should be treated as a value, the only difference with a `val` is that `def` value is dynamically evaluated. An example is scala.Predef.classOf
	- Code block in curly bracket {} is the same as method with no parenthesis, so `def` keyword can be used to make reference to code block
	- Eta-expansion is a simple technique for wrapping functions into an extra layer while preserving identical functionality.
 */
object etaExpansion {
  val a = "foo" // static value of String (evaluated only once)
  def f1 = "f1" // dynamic value of String (evaluated upon every access, i.e. Getter) | f1: String
  def f2() = "f2" // "method" that returns String | f2: ()String

  def gs(s: String): Int = s.length
  def gf(fn: () ⇒ String): Unit = {}
  def gh(fn: ⇒ String): Unit = {}

  gs(f1) // good
//  gf(f1) // not work, as f1 is method instead of function - see below
//  gf(f1()) // error, it's interpreted as `"f1"()`, i.e. `"f1".apply()`

  gs(f2) // interpreted as gs(f2()), and compiler gives a warning “empty-paren method accessed as parameterless”
  gf(f2) // !! ETA-EXPANSION: syntactic sugar of gf({ x:String => f2(x) })

  gs(f2()) // good
//  gf(f2()) // error, type mismatch

  // WARNING “empty-paren method accessed as parameter-less” is given when f and f() have different meaning
  // To spare yourself the confusion, my advice is to always invoke the method as it is defined.
  // If parameter-less, invoke it without parenthesis. If empty-parenthesised, invoke it with empty parenthesis.
  // Former is commonly used for getters of class fields whose value needs to be re-calculated every time (such as currentAccountBalance) and latter for functions with side-effects (such as println()).

  // There are two directions for “eta operation”. In literature, eta-expansion is also called eta-abstraction,
  // whereas opposite direction is called eta-reduction, and they are both referred to under common term eta-conversion.

  // Rule of thumbs: method, not like function, can't be used as parameter

  // Example of eta-expansion
  def someMethod(x: Int, y: String): Unit = ???
  val someFunction: Unit = (x: Int, y: String) ⇒ someMethod(x, y)

  val f1fun1: String = f1 // string, not a function (f1 is evaluated)
//  val f1fun2: () => String = f1 // wrong:  type mismatch
  val f1fun3: () ⇒ String = f1 _ // !!!

  val f2fun1: String = f2 // wrong: string, not a function (with warning)
  val f2fun2: () ⇒ String = f2 // bad: Eta-expansion of zero-argument method values is deprecated in 2.12
  val f2fun3: () ⇒ String = f2 _ // Treat the method as a partially applied function (can be simplified to using the method value itself)

}
