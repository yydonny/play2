package bite

import scala.language.implicitConversions

abstract class Expression {
  def apply(arg: Expression): Expression = Application(this, arg)

  def apply(subst: (Symbol, Expression)): Expression

  def reduced: Expression
}

object Expression {
  private var generatedSymbolCount = 0

  abstract class Abstractable {
    def :->(body: Expression): Lambda
  }

  def free(e: Expression): Set[Symbol] = e match {
    case Operator(v) => Set(v)
    case Lambda(v, body) => free(body) - v
    case Application(f, e0) => free(f) ++ free(e0)
  }

  def λ(v: Symbol)(body: Expression) = Lambda(v, body)

  implicit def λ(f: Operator => Expression): Lambda = {
    generatedSymbolCount += 1
    val s = Symbol("$v" + generatedSymbolCount)
    Lambda(s, f(Operator(s)))
  }

  implicit def symbol2Abstractable(s: Symbol): Abstractable {
    def :->(body: Expression): Lambda
  } = new Abstractable {
    override def :->(body: Expression): Lambda = Lambda(s, body)
  }

  implicit def symbol2Ref(s: Symbol): Operator = Operator(s)
}

case class Operator(v: Symbol) extends Expression {
  override def apply(subst: (Symbol, Expression)): Expression = {
    if (subst._1 == v)
      subst._2
    else
      this
  }

  override def reduced: Operator = this

  override def toString: String = v.name
}

case class Lambda(v: Symbol, body: Expression) extends Expression {
  override def apply(subst: (Symbol, Expression)): Lambda = {
    if (v == subst._1) {
      this
    } else if (Expression.free(body) contains v) {
      throw new Exception(String.format(
        "variable capture while substituting %s for %s in %s",
        subst._2, subst._1, body))
    } else {
      Lambda(v, body(subst))
    }
  }

  override def reduced: Lambda = this

  override def toString: String = String.format("(lambda [%s] %s)", v.name, body)
}

case class Application(f: Expression, e: Expression) extends Expression {
  override def apply(subst: (Symbol, Expression)) = Application(f(subst), e(subst))

  override def reduced: Expression = f.reduced match {
    case Lambda(v, body) => body(v -> e.reduced)
    case _ => this
  }

  override def toString: String = String.format("(%s %s)", f, e)
}


object lambdaCalculus extends App {

  import Expression._

  val id = λ('x)('x)
  val U = λ(f => f(f))
  val U2 = λ { h => h(h) } // same as U
  val U3: Expression = (f: Operator) => f(f) // same, using implicit λ

  println(id)
  println(U(id))
  println(U3)

}
