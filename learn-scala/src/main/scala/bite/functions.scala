package bite

// Various ways of defining function

object Id {
  def apply(x:Int): Int = x
}

object MainF extends App {
  println(this.getClass.getName)
  // function literal
  val id = (x:Int) ⇒ x

  // take a function as parameter and call it. Note the return type
  def id2 (x: ⇒ Int): Int = x
  println(id2({42}))


  // object of an anonymous class with apply method
  // requires runtime reflection
  import scala.language.reflectiveCalls
  val id_a = new {
    def apply(x : Int): Int = x
  }
  println(id_a(434))

  // partial function
  val id_b: Int ⇒ Int = {
    case x if x > 0 ⇒ x
  }

  // curried function
  def f (x: Int)(y: Int) = {
    printf("x = %d, y = %d\n", x, y)
    x + y
  }

  // partly application of curried function
  val f3 = f(3)_
  val f4 = f(_: Int)(4)

  println(f3(4))
  println(f4(3))
}


