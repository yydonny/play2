package bite

object matches extends App{

  // Pattern-matching frequently replaces conditionals in Scala.

  // match against value
  def switch1(x: Any) = x match {
    case 1 => println(1)
    case "3" => println("3")
    case _ => println("other")
  }

  // match against type
  def switch2(x: Any) = x match {
    case _: String =>
      println("String")

    case _: Int =>
      println("Int")
  }

  // Case classes are matchable
  case class Pair(val x: Int, val y: String)
  val p = Pair(42, "forkjoin")
  p match {
    case Pair(43, "forkjoin") => println("Not me.")
    case Pair(42,s) => println("It's " + s + ".")
  }


  
  // unapply() enables objects to be used as match cases
  //  
  // unapply() must return an Option for matching check
  //
  object StartsWith {
    def unapply(s: String): Option[(Char,String)] = s match {
      case "" => None
      case _ => Some((s.charAt(0),s.substring(1)))
    }
  }
  "forkjoin" match {
    case StartsWith('x',rest) => println("Not me.")
    case StartsWith('f',rest) => println("The rest is " + rest + ".")
  }

  // make it cryptic..
  val :+: = StartsWith
  "forkjoin" match {
    case 'f' :+: "ooo" => println("Not me.")
    case 'f' :+: 'o' :+: rest => println("The rest is " + rest + ".")
  }


  // if the result of unapply() is Option[Seq], Option[List], ...
  // then unapplySeq() can be used to destruct the result for pattern-matching
  object Sorted1 {
    def unapply(xs: Seq[Int]) =
      if (xs == xs.sortWith(_ < _)) Some(xs) else None
  }
  object Sorted2 {
    def unapplySeq(xs: Seq[Int]) =
      if (xs == xs.sortWith(_ < _)) Some(xs) else None
  }

  val a = List(1,2,3,4) match {
    case Sorted1(xs) => xs
  }
  val b = List(1,2,3,4) match {
    case Sorted2(a, b, c, d) =>
      (List(a, b), List(c, d))
  }

  println(a) // List(1, 2, 3, 4)
  println(b) // (List(1, 2),List(3, 4))


  object Factor {
    def f(n: Int): List[Int] = n match { // prime factorization of integer n
      case 1 => List.empty
      case n =>
        for (i <- 2 to n) {
          if ((n % i) == 0) {
            return i :: f(n / i)
          }
        }
        return List(n,1) // n is prime
    }

    def unapplySeq(n: Int) = Some(f(n))
  }
  println(Factor.f(6))   // List(2, 3)
  println(Factor.f(20))  // List(2, 2, 5)
  println(Factor.f(50))  // List(2, 5, 5)
  println(Factor.f(120)) // List(2, 2, 2, 3, 5)

  120 match {
    case Factor(a,b,c) => println("Not me.")
    case Factor(a,b,c,d,e) => println((a,c,e))
  }


  // Solve algebra equations by pattern-matching
  object inc {
    def apply(x: Double) = x + 1.0
    def unapply(x: Double) = Some(x - 1.0)
  }

  object sqr {
    def apply(x: Double) = x * x
    def unapply(x: Double) = Some(Math.sqrt(x))
  }

  // solve: x^2 + 1 = y
  def equation1(y: Double) = y match {
    case inc(sqr(x)) => x
  }

  println(equation1(145)) // 12.0


  // generate patterns at runtime
  def needMore(n: Int) = new Object {
    def unapply(total: Int) = Some(total - n)
  }

  val need42 = needMore(42)
  100 match {
    case need42(x) => println(x) // 58
  }
}
