package bite

import scala.math.BigInt

// various factorial implementation

object factorials {
  def vanilla(n: BigInt): BigInt = {
    var i = n
    var a: BigInt = 1
    while (i > 0) {
      a = a * i
      i -= 1
    }
    a
  }

  // naive recursive
  def factr(n: BigInt): BigInt = {
    if (n <= 0) 1 else n * factr(n - 1)
  }

  def tailRecursive(i: BigInt): BigInt = { // tail recursive
    def ft(i: BigInt, acc: BigInt): BigInt = if (i == 0) acc else ft(i - 1, i * acc)
    ft(i, 1)
  }

  def f4(i: BigInt): BigInt = i match { // BigInt is not a case class
    case i:BigInt =>
      if (i < 2) 1 else i * f4(i - 1)
  }

  // vanilla, fancy
  def f5(i: Int): Int = ((1 to i) foldRight 1)(_ * _) // curried function
  def f6(i: Int): Int = (1 to i).product // same as f5
  def f7(i: Int): Int = ((1 to i) :\ 1)(_ * _) // same as f5

}


object Mainf2 extends App {
  println(factorials.f5(10))
}






