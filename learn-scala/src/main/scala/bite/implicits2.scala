package bite

import scala.collection.immutable.SortedMap
import scala.collection.immutable.TreeMap

case class Person(ssn : Int, name : String)

object implicits2 extends App {
  //Error:(9, 62) No implicit Ordering defined for bite.Person.
//  val db1 : SortedMap[Person,Symbol] = TreeMap[Person,Symbol]()

  // Implicitly converts a Person to an Ordered[Person]
  implicit object ooo extends Ordering[Person] {
    def compare (p1 : Person, p2 : Person) : Int = p1.ssn - p2.ssn
  }

  // ok here
  val db1 : SortedMap[Person,Symbol] = TreeMap[Person,Symbol]()
  val db2 = db1 + (Person(1, "Matt") → 'Chicken) + (Person(2, "Matt") → 'Mouse)
  println(db2)

  // another order
  // no implicit here. otherwise causing ambiguous implicit error
  object OrderingByName extends Ordering[Person] {
    def compare (p1 : Person, p2 : Person) : Int = p1.name compare p2.name
  }

  val dbX : SortedMap[Person,Symbol] = TreeMap[Person,Symbol]()(OrderingByName) // explicitly use the order.
  val dbY = dbX + (Person(1, "Matt") → 'Chicken)
  val dbZ = dbY + (Person(2, "Matt") → 'Mouse)
  println(dbZ)
}
