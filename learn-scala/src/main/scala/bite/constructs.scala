package bite

import scala.annotation.tailrec

// homegrown while loop

object constructs extends App {
  @tailrec def while1 (c: => Boolean)(fn: => Unit): Unit = {
    if (c) {
      fn
      while1(c)(fn)
    }
  }

  var i = 1
  while1(i < 10) {
    println(i)
    i += 1
  }

  def benchmark (body : => Unit) : Long = {
    val start = java.util.Calendar.getInstance().getTimeInMillis()
    body
    val end = java.util.Calendar.getInstance().getTimeInMillis()
    end - start
  }

  val myTime = benchmark {
    var i = 0 ;
    while1 (i < 1000000) {
      i += 1 ;
    }
  }

  println("my while1 took: " + myTime)

  val time = benchmark {
    var i = 0 ;
    while (i < 1000000) {
      i += 1 ;
    }
  }

  println("while took:   " + time)
}
