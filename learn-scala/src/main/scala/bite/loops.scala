package bite

class Ship(val x: Int, val y: Int) extends Iterable[(String, Int)] {
  override def iterator = new Iterator[(String, Int)] {
    private var nextVar = 'x

    override def hasNext: Boolean = nextVar != 'z

    override def next(): (String, Int) = nextVar match {
      case 'x =>
        nextVar = 'y
        ("x", x)
      case 'y =>
        nextVar = 'z
        ("y", y)
    }
  }
}


object loops extends App {
  val l = List(1,2,3,4,5,6,7)
  var j = 0
  while (j < l.size) {
    println(l(j))
    j += 1
  }

  // don't need to declare i
  for (i <- 0 until l.size) {
    println(l(i))
  }

  for (e <- l) {
    println(e)
  }

  l.foreach((e:Int) => println(e)) // must use parentheses if type is given
  l.foreach(e => println(e))
  l.foreach(println _)
  l.foreach(println) // use function as argument
  l foreach println


  // The following are ways to copy an array

  val m = new Array[Int](l.length)

  for (i <- 0 until m.length) {
    m.update(i, l(i))
    // or
    m(i) = l(i)
  }

  (0 until l.length).foreach(i => m(i) = l(i))
  (0 until l.length).foreach { i => m(i) = l(i) }

  // unshift
  // method :: belongs to List (method ends with ':')
  println(0 :: l)

  // map function
  def map1[A,B](fn: A => B)(list: List[A]): List[B] = {
    if (list.isEmpty) {
      Nil
    } else {
      fn(list.head) :: map1(fn)(list.tail)
    }
  }

  // must explicitly give type
  // map1(2 * _)( List[Nothing] )
  // map1((i:Int) => i * 2)( List[Int] )
  (map1((i:Int) => i * 2)(l)) foreach println

  // the official map() is more powerful...
  m.map(i => i * 1) foreach println

  m map {_ + 1} foreach println


  val s = new Ship(42, 1701)
  for ((c, v) <- s) {
    printf("%s: %d\n", c, v)
  }

}
