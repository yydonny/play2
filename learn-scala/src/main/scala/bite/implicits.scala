package bite

import scala.language.{implicitConversions, postfixOps}

// A dummy RegEx implementation showing usage of case class and implicits

abstract class RegEx {
  def ~(right: RegEx) = Sequence(this, right)

  def |-(right: RegEx) = Alternation(this, right)

  def * = Repetition(this)

  def matchesString(s: String): Boolean =
    throw new Exception("An exercise for the reader!")
}

// Charaters:
case class CharEx(c: Char) extends RegEx

// Sequences:
case class Sequence(left: RegEx, right: RegEx) extends RegEx

// Alternation:
case class Alternation(left: RegEx, right: RegEx) extends RegEx

// Kleene repetition:
case class Repetition(exp: RegEx) extends RegEx

// Empty:
case object Empty extends RegEx

object implicits extends App {

  // Automatically convert strings into regexes:
  implicit def toRegex(s: String): RegEx = {
    var ex: RegEx = Empty
    for (c ← s) { ex = Sequence(ex, CharEx(c)) }
    ex
  }


  // Building regexes manually is cumbersome:
  val rx1 = Sequence(CharEx('f'), Sequence(CharEx('o'), CharEx('o')))

  // Implicits + operator overloading makes the syntax terse:
  val rx2 = "baz" ~ ("forkjoin" |- "bar") *;
  println(rx2)

  implicit def a2opt[A](a: A): Option[A] = Option(a)
  println(77 map { _ + 3 } getOrElse 42) // 80

}
