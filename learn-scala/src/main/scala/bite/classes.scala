package bite

class D(var x: Int)

object dr extends D(4) // Object inheritance
                       // using lower case 'd' as name causes class name conflict here

class E(private val x: Int) // x not visible

class F {
  private var _x = 0;
  def x = _x                 // getter
  def x_= (x: Int): Unit = { // setter
    this._x = x
  }
}

class G {
  lazy val x = {
    println("init-on-demand field")
    42
  }
}


class A
class C1(x: Int) // no field x defined
class C2(val x: Int)
object C2 {      // Use companion object to define what would be static fields in java.
  def create(x:Int) = new C2(x)
}

case class H(x: Int) extends A
case class I(s: String) extends A

object MainC extends App {
  println(this.getClass.getName)

  println(new C1(3))
  println(C2.create(2).x)
  println(new D(3).x)
  println(new E(3)) // x not visible

  println(dr.getClass.getName)
  println(dr.x)

  val f = new F
  f.x = 3
  println(f.x)

  val match1: A ⇒ AnyRef = { // case class for pattern matching
                              // must give the type and signature explicitly for partial function
    case H(3) => "hhh-3: match class and value"
    case H(_) => "hhh: only care about class"
//    case H(4) => "hhh-4: match class and value <--- warning: UNREACHABLE BRANCH"
    case I(n) => "iii, also catch the value matched: " + n
  }

  println(match1.getClass)
  println(match1(H(2))) // case class doesn't need new
  println(match1(H(3)))
  println(match1(H(4)))
  println(match1(I("12")))
}

