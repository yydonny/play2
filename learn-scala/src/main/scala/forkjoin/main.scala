package forkjoin

import java.util.concurrent.ForkJoinPool

import scala.util.Random

object main extends App {
  val input = Seq.newBuilder[Int]
  100 until 356 foreach { i =>
    input += Random.nextInt(i)
  }

  val fj = new FJSorter[Int](Random.shuffle(input.result))
  new ForkJoinPool(4).invoke(fj)
  println(fj.items.mkString(","))
}
