package forkjoin

import java.util.concurrent.{ForkJoinTask, RecursiveAction}

import scala.collection.mutable.ArrayBuffer

object FJSorter {
  val SMALL_ENOUGH = 32
}

/**
  * Multithreaded Merge Sort using ForkJoinPool
  * @param _items
  * @param ord
  * @tparam T
  */
class FJSorter[T](_items: Seq[T])(implicit ord: Ordering[T])
  extends RecursiveAction
{
  private var buffer: Seq[T] = _items

  def items = buffer
  def size = items.length

  /**
    * Merge left and right, replace the buffer of this sorter with the result
    * @param left
    * @param right
    */
  def merge(left: FJSorter[T], right: FJSorter[T]): Unit = {
    import ord._
    val newBuf = Seq.newBuilder[T]
    var (il, ir) = (0, 0)
    while (il < left.size && ir < right.size) {
      if (left.items(il) < right.items(ir)) {
        newBuf += left.items(il)
        il += 1
      } else {
        newBuf += right.items(ir)
        ir += 1
      }
    }
    while (il < left.size) {
      newBuf += left.items(il)
      il += 1
    }
    while (ir < right.size) {
      newBuf += right.items(ir)
      ir += 1
    }
    buffer = newBuf.result
  }

  override def compute(): Unit = {
    if (size < FJSorter.SMALL_ENOUGH) {
      buffer = buffer.sorted
    } else {
      val halves = items splitAt (size / 2)
      val left = new FJSorter(halves._1)
      val right = new FJSorter(halves._2)
      ForkJoinTask.invokeAll(left, right)
      merge(left, right)
    }
  }
}
