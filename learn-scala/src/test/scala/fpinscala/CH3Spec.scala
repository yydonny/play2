package fpinscala

import org.scalatest.FunSpec

class CH3Spec extends FunSpec {
  describe("Practices in Chapter 3") {
    describe("Methods of Cons/ListI") {
      it("ListI.sum") {
        assert(ListI.sum(ListI(1,2,3,4,5)) == 15)
      }

      it("ListI.length") {
        assert(ListI.length(ListI(1,2,3,4,5)) == 5)
      }

      it("ListI.append") {
        assert(ListI.append(ListI(1,2,3,4), 5) == ListI(1,2,3,4,5))
      }

      it("ListI.flatten") {
        val a = ListI(1,2,3)
        val b = ListI(4,5,6)
        assert(ListI.flatten(ListI(a, b)) == ListI(1,2,3,4,5,6))
      }

      it("ListI.map") {
        assert(ListI.map(ListI(1,2,3))(2 * _) == ListI(2,4,6))
      }

      it("ListI.flatMap") {
        assert(ListI.flatMap(ListI(1,2,3)) { x => ListI(x, x) } == ListI(1,1,2,2,3,3))
      }
    }
  }
}
