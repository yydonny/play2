package yangyd.play2.resourceserver.web

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RestController}
import yangyd.play2.resourceserver.service.NettyHttpClientDemo

@RestController
@RequestMapping(Array("/bar"))
class DemoController @Autowired()(nettyHttpClientDemo: NettyHttpClientDemo) {
  @RequestMapping
  def foo = {
    nettyHttpClientDemo.test1()
    "ok"
  }
}
