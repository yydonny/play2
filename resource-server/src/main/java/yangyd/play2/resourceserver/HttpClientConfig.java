package yangyd.play2.resourceserver;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import org.apache.http.client.HttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import yangyd.anthology.jsse.ApacheHttpClientBuilder;
import yangyd.anthology.jsse.SSLContextBuilder;
import yangyd.anthology.netty.NettyHttpClient;

import javax.net.ssl.SSLContext;
import java.io.FileInputStream;
import java.io.IOException;

@Configuration
class HttpClientConfig {

  @Bean
  NettyHttpClient httpClient() {
    return new NettyHttpClient(nioEventLoopGroup(), NioSocketChannel.class, nettySSLContext());
  }

  @Bean
  RestTemplate restTemplate() {
    return new RestTemplate(requestFactory());
  }

  @Bean
  ClientHttpRequestFactory requestFactory() {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
    factory.setHttpClient(apacheHttpClient());
    return factory;
  }

  @Bean
  HttpClient apacheHttpClient() {
    return new ApacheHttpClientBuilder(sslContext()).build();
  }

  @Bean
  SSLContext sslContext() {
    return sslContextBuilder().buildForJSSE();
  }

  @Bean
  SSLContextBuilder sslContextBuilder() {
    SSLContextBuilder builder = new SSLContextBuilder();
    builder.setClientKeyPass(dangerous.jksPassword());
    try {
      builder.loadClientKeyStore(new FileInputStream(dangerous.jksFile()));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return builder;
  }

  private EventLoopGroup nioEventLoopGroup() {
    return new NioEventLoopGroup();
  }

  @Bean
  SslContext nettySSLContext() {
    return sslContextBuilder().buildForNetty();
  }
}
