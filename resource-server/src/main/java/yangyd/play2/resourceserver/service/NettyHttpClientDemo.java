package yangyd.play2.resourceserver.service;

import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.util.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import yangyd.anthology.netty.NettyHttpClient;
import yangyd.anthology.netty.NettyHttpRequestSpecBuilder;
import yangyd.play2.resourceserver.dangerous;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Component
public class NettyHttpClientDemo {
  @Autowired
  private NettyHttpClient httpClient;

  public void test1() {
    NettyHttpRequestSpecBuilder builder = dangerous.requestBuilder3();
    debugStream(builder);
    builder.onFullResponse(NettyHttpClientDemo::debug);
    httpClient.execute(builder.buildForDownload());
  }

  public void download() {
    NettyHttpRequestSpecBuilder builder = buildBase(HttpMethod.POST,
          dangerous.testHost(), dangerous.testPort(), dangerous.testPath())
        .body(dangerous.testJSON().getBytes());
    debugStream(builder);
    httpClient.execute(builder.buildForDownload());
  }

  public void simple() {
    NettyHttpRequestSpecBuilder builder = buildBase(HttpMethod.POST,
        dangerous.testHost(), dangerous.testPort(), dangerous.testPath())
        .body(dangerous.testJSON().getBytes());
    builder.onFullResponse(NettyHttpClientDemo::debug);
    httpClient.execute(builder.build());
  }

  public void upload() {
    NettyHttpRequestSpecBuilder builder = buildBase(HttpMethod.POST,
        dangerous.testHost(), dangerous.testPort(), dangerous.testPath());
    builder.header(HttpHeaders.Names.CONTENT_TYPE, "application/octet-stream");
    try {
      builder.body(new FileInputStream(dangerous.testFile()));
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    builder.onFullResponse(NettyHttpClientDemo::debug);
    httpClient.execute(builder.build());
  }


  private NettyHttpRequestSpecBuilder buildBase(HttpMethod method, String host, int port, String path) {
    NettyHttpRequestSpecBuilder builder = new NettyHttpRequestSpecBuilder();
    builder.method(method).host(host).port(port).path(path)
        .header(HttpHeaders.Names.CONTENT_TYPE, HttpHeaders.Values.APPLICATION_JSON);
    return builder;
  }

  private void debugStream(NettyHttpRequestSpecBuilder builder) {
    builder.onResponse(message -> {
      debug(message);
      System.err.println("CONTENT >>>");
    });
    builder.onData(byteBuf -> System.err.print(byteBuf.toString(CharsetUtil.UTF_8)));
    builder.onEnd(() -> System.err.println("\n<<< END OF CONTENT"));
    builder.onError(Throwable::printStackTrace);
  }

  private static void debug(HttpResponse message) {
    DecoderResult decoderResult = message.getDecoderResult();
    System.err.println("decoderResult.isFinished: " + decoderResult.isFinished());
    System.err.println("decoderResult.isSuccess: " + decoderResult.isSuccess());
    System.err.println("decoderResult.isFailure: " + decoderResult.isFailure());
    System.err.println("STATUS: " + message.getStatus());
    if (!message.headers().isEmpty()) {
      for (String name : message.headers().names()) {
        for (String value : message.headers().getAll(name)) {
          System.err.println("HEADER: " + name + " = " + value);
        }
      }
    }
  }
}
