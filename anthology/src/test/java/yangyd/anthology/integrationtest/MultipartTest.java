package yangyd.anthology.integrationtest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import yangyd.anthology.web.NamedInputStreamResource;

import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MultipartTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void test1() {
    MultiValueMap<String, Object> payload = new LinkedMultiValueMap<>();
    payload.add("name 1", "value 1");
    payload.add("name 2", "value 2+1");
    payload.add("name 2", "value 2+2");
    payload.add("file", new NamedInputStreamResource(testFile(), "test.xlsx"));
    payload.add("xml", new StreamSource(new StringReader("<root><child/></root>")));

    ResponseEntity<List> response = restTemplate.postForEntity("/multipart", payload, List.class);
    Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assert.assertTrue(String.valueOf(response.getBody().get(0)).endsWith("test.xlsx"));
  }

  private InputStream testFile() {
    return MultipartTest.class.getClassLoader().getResourceAsStream("5061-bytes.dat");
  }
}
