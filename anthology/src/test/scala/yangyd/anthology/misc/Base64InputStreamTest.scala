package yangyd.anthology.misc

import java.io._
import java.net.URL
import java.nio.file.{Files, Paths}
import java.util.Base64

import org.apache.tomcat.util.http.fileupload.IOUtils
import org.scalatest._

object Base64InputStreamTest {
  val base64Encoder: Base64.Encoder = Base64.getEncoder
  val chunk5059 = classOf[Base64InputStreamTest].getClassLoader.getResource("5059-bytes.dat")
  val chunk5060 = classOf[Base64InputStreamTest].getClassLoader.getResource("5060-bytes.dat")
  val chunk5061 = classOf[Base64InputStreamTest].getClassLoader.getResource("5061-bytes.dat")
}

class Base64InputStreamTest extends UnitSpec {

  import Base64InputStreamTest._
  import scala.language.reflectiveCalls
  def fixture = new { // structured type, need to enable reflectiveCalls
    val base64Data5059 = base64Encoder.encode(Files.readAllBytes(Paths.get(chunk5059.toURI)))
    val base64Data5060 = base64Encoder.encode(Files.readAllBytes(Paths.get(chunk5060.toURI)))
    val base64Data5061 = base64Encoder.encode(Files.readAllBytes(Paths.get(chunk5061.toURI)))
  }

  def base64test(url: URL, refBase64: Array[Byte]): Unit = {
    val buf = new ByteArrayOutputStream()
    IOUtils.copy(new Base64InputStream(new FileInputStream(new File(url.toURI))), buf)
    assert(buf.size() === refBase64.length)
    assert(buf.toByteArray === refBase64)
  }

  "Base64InputStream" should "correctly encode data chunk of 5059 bytes" in base64test(chunk5059, fixture.base64Data5059)

  it should "correctly encode data chunk of 5060 bytes" in base64test(chunk5060, fixture.base64Data5060)

  it should "correctly encode data chunk of 5061 bytes" in base64test(chunk5061, fixture.base64Data5061)

}
