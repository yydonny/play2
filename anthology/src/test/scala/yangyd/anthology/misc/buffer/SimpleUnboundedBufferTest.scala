package yangyd.anthology.misc.buffer

import java.nio.ByteBuffer
import java.nio.charset.Charset

import yangyd.anthology.misc.UnitSpec

object SimpleUnboundedBufferTest {
  val chunk1 = "春は曙、やう白くなりゆく山際すこしあかりて、紫だちたる雲の細くたなびきたる。"
  val chunk2 = "夏は夜、月の頃はさらなり、闇もなほ螢飛びちがひたる、雨などの降るさへをかし。"
  val chunk3 = "秋は夕暮、夕日はなやかにさして、山の端いと近くなりたるに、烏のねどころへ行くとて、三つ四つ二つなンど飛びゆくさへあはれなり。"
  val sources = Seq(chunk1, chunk2, chunk3)
  val UTF_8 = Charset.forName("UTF-8")
}

class SimpleUnboundedBufferTest extends UnitSpec {
  import SimpleUnboundedBufferTest._
  val testee = new SimpleUnboundedBuffer

  def utf8bytes(s: String) = s.getBytes(UTF_8)

  "SimpleUnboundedBuffer" should "accept ByteBuffer to be appended" in {
    sources.map(utf8bytes).map(ByteBuffer.wrap).foreach(testee.append)
    assert(testee.available() === sources.map(utf8bytes(_).length).sum)
  }

  it should "read 1 byte with get()" in {
    val bytes1 = utf8bytes(chunk1)
    testee.append(ByteBuffer.wrap(bytes1))
    assert(testee.get() === bytes1(0))
    assert(testee.get() === bytes1(1))
    assert(testee.get() === bytes1(2))
    assert(testee.get() === bytes1(3))
    assert(testee.get() === bytes1(4))
  }
}
