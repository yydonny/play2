package yangyd.anthology.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

class NettyHttpRequests {
  private static final String USER_AGENT = "anthology-http-client 1.0";

  static HttpRequest chunkedRequest(HttpMethod method, String host, String uri) {
    HttpRequest request = baseRequest(method, host, uri);
    request.headers().set(HttpHeaders.Names.TRANSFER_ENCODING, HttpHeaders.Values.CHUNKED);
    return request;
  }

  static HttpRequest baseRequest(HttpMethod method, String host, String uri, byte[] body) {
    if (body == null) {
      return baseRequest(method, host, uri);
    } else {
      ByteBuf payload = wrapped(body);
      FullHttpRequest fullHttpRequest = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, method, uri, payload, true);
      HttpHeaders headers = fullHttpRequest.headers();
      setBaseHeaders(headers, host);
      headers.add(HttpHeaders.Names.CONTENT_LENGTH, payload.readableBytes());
      return fullHttpRequest;
    }
  }

  private static HttpRequest baseRequest(HttpMethod method, String host, String uri) {
    HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, method, uri);
    setBaseHeaders(request.headers(), host);
    return request;
  }

  private static void setBaseHeaders(HttpHeaders headers, String host) {
    headers.set(HttpHeaders.Names.HOST, host);
    headers.set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
    headers.set(HttpHeaders.Names.USER_AGENT, USER_AGENT);
  }

  private static ByteBuf wrapped(byte[] bytes) {
    ByteBuf buf = Unpooled.wrappedBuffer(bytes);
    buf.readerIndex(0);
    buf.writerIndex(bytes.length);
    return buf;
  }

  static String encodeURI(String rawPath, Map<String, String> query) throws URISyntaxException {
    if (query == null || query.isEmpty()) {
      return new URI(rawPath).toASCIIString();
    } else {
      QueryStringEncoder encoder = new QueryStringEncoder(rawPath);
      for (Map.Entry<String, String> pair : query.entrySet()) {
        encoder.addParam(pair.getKey(), pair.getValue());
      }
      return new URI(encoder.toString()).toASCIIString();
    }
  }

}
