package yangyd.anthology.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.SslHandshakeCompletionEvent;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;

import javax.net.ssl.SSLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class NettyHttpClient {
  private static final int READ_TIMEOUT_SECONDS = 30;
  private static final int MAX_CONTENT_LENGTH = 1024 * 64;
  private static final int CONNECT_TIMEOUT_MILLIS = 30 * 1000;
  private final Bootstrap bootstrap;
  private final SslContext sslContext;

  public NettyHttpClient(EventLoopGroup eventLoopGroup, Class<? extends SocketChannel> channelClass) {
    this(eventLoopGroup, channelClass, null);
  }

  public NettyHttpClient(EventLoopGroup eventLoopGroup, Class<? extends SocketChannel> channelClass, SslContext sslContext) {
    this.sslContext = sslContext;
    bootstrap = new Bootstrap();
    bootstrap.group(eventLoopGroup);
    bootstrap.channel(channelClass);
    bootstrap.handler(new HttpChannelInitializer());
  }

  public void execute(NettyHttpRequestSpec spec) {
    List<ChannelHandler> handlers = acquireHandlers(spec);
    execute0(spec, handlers.toArray(new ChannelHandler[handlers.size()]));
  }

  private static List<ChannelHandler> acquireHandlers(NettyHttpRequestSpec spec) {
    List<ChannelHandler> handlers = new LinkedList<>();

    if (FullHttpResponse.class.equals(((TypedHandler<?>) spec.inboundHandler).getMessageType())) {
      handlers.add(new HttpObjectAggregator(MAX_CONTENT_LENGTH));
    }

    handlers.add(spec.inboundHandler);

    Optional<ChannelOutboundHandler> streaming = spec.streaming();
    if (streaming.isPresent()) {
      handlers.add(new ChunkedWriteHandler());
      handlers.add(streaming.get());
    }
    return handlers;
  }

  private void execute0(NettyHttpRequestSpec spec, ChannelHandler... handlers) {
    bootstrap.connect(spec.host, spec.port).addListener((ChannelFutureListener) future -> {
      if (future.isSuccess()) {
        Channel channel = future.channel();
        ChannelPipeline pipeline = channel.pipeline();
        if (sslContext != null) {
          pipeline.addFirst(
              new SslHandler(sslContext.newEngine(channel.alloc())),
              new CatchHandshakeError());
        }
        pipeline.addLast(handlers);
        pipeline.addLast(new ReadTimeoutHandler(READ_TIMEOUT_SECONDS));
        channel.writeAndFlush(spec.request);
      } else {
        spec.onError.accept(future.cause());
      }
    });
  }

  /**
   * Base setup for every channel.
   */
  private static class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
      ch.config().setConnectTimeoutMillis(CONNECT_TIMEOUT_MILLIS);
      ch.pipeline().addLast(new HttpClientCodec());
    }
  }

  private static class CatchHandshakeError extends ChannelInboundHandlerAdapter {
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
      if (evt instanceof SslHandshakeCompletionEvent) {
        SslHandshakeCompletionEvent hce = (SslHandshakeCompletionEvent) evt;
        if (!hce.isSuccess()) {
          ctx.fireExceptionCaught(new SSLException("SSL handshake failed! " + hce, hce.cause()));
        }
      }
    }
  }
}
