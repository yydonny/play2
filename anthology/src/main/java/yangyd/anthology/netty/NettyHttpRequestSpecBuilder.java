package yangyd.anthology.netty;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.*;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class NettyHttpRequestSpecBuilder {
  private HttpMethod method = Defaults.method;
  private String host;
  private int port = Defaults.port;
  private String path = Defaults.path;
  private final Map<String, String> queries = new HashMap<>();
  private final HttpHeaders headers = new DefaultHttpHeaders();

  private byte[] body;
  private InputStream streamingBody;

  private Consumer<Throwable> onError = Defaults.onError;
  private Consumer<FullHttpResponse> onFullResponse;
  private Consumer<HttpResponse> onResponse = Defaults.onResponse;
  private Consumer<ByteBuf> onData = Defaults.onData;
  private Runnable onEnd = Defaults.onEnd;

  /**
   * Build the request spec for netty client. {@code onFullResponse} will be invoked when the response is received.
   */
  public NettyHttpRequestSpec build() {
    if (host == null) {
      throw new IllegalArgumentException("host is not specified");
    }

    if (streamingBody == null) {
      return new NettyHttpRequestSpec(host, port, buildRequest(false), onError, onFullResponse);
    } else {
      return new NettyHttpRequestSpec(host, port, buildRequest(true), onError, onFullResponse, streamingBody);
    }
  }

  /**
   * Build the request spec for netty client. {@code onResponse}, {@code onData}, and {@code onEnd} will be invoked to handle the response stream.
   * @return the request spec
   */
  public NettyHttpRequestSpec buildForDownload() {
    if (host == null) {
      throw new IllegalArgumentException("host is not specified");
    }

    if (streamingBody == null) {
      return new NettyHttpRequestSpec(host, port, buildRequest(false), onError, new InboundCallback(onResponse, onData, onEnd));
    } else {
      return new NettyHttpRequestSpec(host, port, buildRequest(true), onError, new InboundCallback(onResponse, onData, onEnd), streamingBody);
    }
  }

  private HttpRequest buildRequest(boolean chunked) {
    try {
      HttpRequest request;
      String uri = NettyHttpRequests.encodeURI(path, queries);
      if (chunked) {
        request = NettyHttpRequests.chunkedRequest(method, host, uri);
      } else {
        request = NettyHttpRequests.baseRequest(method, host, uri, body);
      }
      request.headers().add(headers);
      return request;
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException("path or query is not valid.", e);
    }
  }

  public NettyHttpRequestSpecBuilder header(String name, Object value) {
    headers.add(name, value);
    return this;
  }

  public NettyHttpRequestSpecBuilder query(String name, String value) {
    queries.put(name, value);
    return this;
  }

  public NettyHttpRequestSpecBuilder method(HttpMethod method) {
    this.method = method;
    return this;
  }

  public NettyHttpRequestSpecBuilder host(String host) {
    this.host = host;
    return this;
  }

  public NettyHttpRequestSpecBuilder port(int port) {
    this.port = port;
    return this;
  }

  public NettyHttpRequestSpecBuilder path(String path) {
    this.path = path;
    return this;
  }

  public NettyHttpRequestSpecBuilder body(byte[] body) {
    this.body = body;
    return this;
  }

  public NettyHttpRequestSpecBuilder body(InputStream chunkedBody) {
    this.streamingBody = chunkedBody;
    return this;
  }

  public NettyHttpRequestSpecBuilder onError(Consumer<Throwable> onError) {
    this.onError = onError;
    return this;
  }

  public NettyHttpRequestSpecBuilder onFullResponse(Consumer<FullHttpResponse> onFullResponse) {
    this.onFullResponse = onFullResponse;
    return this;
  }

  public NettyHttpRequestSpecBuilder onResponse(Consumer<HttpResponse> onResponse) {
    this.onResponse = onResponse;
    return this;
  }

  public NettyHttpRequestSpecBuilder onData(Consumer<ByteBuf> onData) {
    this.onData = onData;
    return this;
  }

  public NettyHttpRequestSpecBuilder onEnd(Runnable onEnd) {
    this.onEnd = onEnd;
    return this;
  }

  private interface Defaults {
    HttpMethod method = HttpMethod.GET;
    int port = 80;
    String path = "/";

    Consumer<Throwable> onError = Throwable::printStackTrace;
    Consumer<HttpResponse> onResponse = o -> {};
    Consumer<ByteBuf> onData = o -> {};
    Runnable onEnd = () -> {};
  }
}
