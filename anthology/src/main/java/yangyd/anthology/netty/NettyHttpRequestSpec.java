package yangyd.anthology.netty;

import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.handler.stream.ChunkedStream;

import java.io.InputStream;
import java.util.Optional;
import java.util.function.Consumer;

public class NettyHttpRequestSpec {
  final String host;
  final int port;
  final HttpRequest request;
  final Consumer<Throwable> onError;

  private final ChannelOutboundHandler outboundHandler;
  final ChannelInboundHandler inboundHandler;

  NettyHttpRequestSpec(String host, int port, HttpRequest request,
                       Consumer<Throwable> onError,
                       Consumer<FullHttpResponse> onFullResponse) {
    this(host, port, request, onError, onFullResponse, null);
  }

  NettyHttpRequestSpec(String host, int port, HttpRequest request,
                       Consumer<Throwable> onError,
                       InboundCallback inboundCallback) {
    this(host, port, request, onError, inboundCallback, null);
  }

  NettyHttpRequestSpec(String host, int port, HttpRequest request,
                       Consumer<Throwable> onError,
                       Consumer<FullHttpResponse> onFullResponse,
                       InputStream inputStream) {
    inboundHandler = new FullResponseHandler(onFullResponse);

    this.host = host;
    this.port = port;
    this.request = request;
    this.onError = onError;
    outboundHandler = inputStream == null ? null : new SendDataHandler(inputStream);
  }

  NettyHttpRequestSpec(String host, int port, HttpRequest request,
                       Consumer<Throwable> onError,
                       InboundCallback inboundCallback,
                       InputStream inputStream) {
    inboundHandler = new StreamResponseHandler(inboundCallback);

    this.host = host;
    this.port = port;
    this.request = request;
    this.onError = onError;
    outboundHandler = inputStream == null ? null : new SendDataHandler(inputStream);
  }

  Optional<ChannelOutboundHandler> streaming() {
    return Optional.ofNullable(outboundHandler);
  }

  private class FullResponseHandler extends SimpleChannelInboundHandler<FullHttpResponse>
      implements TypedHandler<FullHttpResponse>
  {
    private final Consumer<FullHttpResponse> onResponse;

    private FullResponseHandler(Consumer<FullHttpResponse> onResponse) {
      this.onResponse = onResponse;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext context, FullHttpResponse response) throws Exception {
      onResponse.accept(response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) throws Exception {
      onError.accept(cause);
    }

    @Override
    public Class<FullHttpResponse> getMessageType() {
      return FullHttpResponse.class;
    }
  }

  private class StreamResponseHandler extends SimpleChannelInboundHandler<HttpObject>
      implements TypedHandler<HttpObject>
  {
    private final InboundCallback inboundCallback;

    StreamResponseHandler(InboundCallback inboundCallback) {
      this.inboundCallback = inboundCallback;
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, HttpObject msg) {
      if (msg instanceof HttpResponse) {
        inboundCallback.onResponse.accept((HttpResponse) msg);

      } else if (msg instanceof HttpContent) {
        HttpContent chunk = (HttpContent) msg;
        inboundCallback.onData.accept(chunk.content());

        if (chunk instanceof LastHttpContent) {
          inboundCallback.onEnd.run();
          ctx.channel().close();
        }

      } else {
        throw new IllegalStateException("Unexpected message: " + msg); // should not happen
      }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
      onError.accept(cause);
    }

    @Override
    public Class<HttpObject> getMessageType() {
      return HttpObject.class;
    }
  }

  private class SendDataHandler extends ChannelOutboundHandlerAdapter {
    private final InputStream stream;

    private SendDataHandler(InputStream stream) {
      this.stream = stream;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
      Channel channel = ctx.channel();
      channel.writeAndFlush(new ChunkedStream(stream));
      channel.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
      super.handlerAdded(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
      onError.accept(cause);
      super.exceptionCaught(ctx, cause);
    }
  }

}
