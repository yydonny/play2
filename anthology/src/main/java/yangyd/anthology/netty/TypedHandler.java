package yangyd.anthology.netty;

import io.netty.handler.codec.http.HttpObject;

interface TypedHandler<T extends HttpObject> {
  Class<T> getMessageType();
}
