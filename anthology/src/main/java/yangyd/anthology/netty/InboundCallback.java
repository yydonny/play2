package yangyd.anthology.netty;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpResponse;

import java.util.function.Consumer;

class InboundCallback {
  final Consumer<HttpResponse> onResponse;
  final Consumer<ByteBuf> onData;
  final Runnable onEnd;

  InboundCallback(Consumer<HttpResponse> onResponse, Consumer<ByteBuf> onData, Runnable onEnd) {
    this.onResponse = onResponse;
    this.onData = onData;
    this.onEnd = onEnd;
  }
}
