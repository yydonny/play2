package yangyd.anthology.jsse;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class StrategyBasedTrustManager implements X509TrustManager {
  private final X509TrustManager delegate;
  private final TrustStrategy trustStrategy;

  public StrategyBasedTrustManager(X509TrustManager delegate, TrustStrategy trustStrategy) {
    super();
    this.delegate = delegate;
    this.trustStrategy = trustStrategy;
  }

  public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    delegate.checkClientTrusted(chain, authType);
  }

  public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    if (!trustStrategy.isTrusted(chain, authType)) {
      delegate.checkServerTrusted(chain, authType);
    }
  }

  public X509Certificate[] getAcceptedIssuers() {
    return delegate.getAcceptedIssuers();
  }
}
