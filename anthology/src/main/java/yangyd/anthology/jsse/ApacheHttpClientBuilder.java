package yangyd.anthology.jsse;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Simplified builder for SSL-capable http client of the Apache HTTP Component.
 * By default TLS 1.1 and 1.2 is supported, less secure TLS 1.0 and SSL 3 can be also enabled manually.
 * The Hostname is not verified by default, user should provide its own {@link HostnameVerifier} implementation.
 */
public class ApacheHttpClientBuilder {
  // @see sun.security.ssl.ProtocolVersion
  private static final String TLS_VERSION_1 = "TLSv1";
  private static final String TLS_VERSION_1_1 = "TLSv1.1";
  private static final String TLS_VERSION_1_2 = "TLSv1.2";
  private static final String SSL_VERSION_3 = "SSLv3";

  private static final HostnameVerifier INSECURE_VERIFIER = (s, sslSession) -> true;

  private final SSLContext sslContext;
  private final Set<String> protocols = new LinkedHashSet<>();
  private HostnameVerifier hostnameVerifier = INSECURE_VERIFIER;
  private String[] cipherSuites;

  public ApacheHttpClientBuilder(SSLContext sslContext) {
    this.sslContext = sslContext;
    protocols.add(TLS_VERSION_1_1);
    protocols.add(TLS_VERSION_1_2);
  }

  public CloseableHttpClient build() {
    SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(
        sslContext,
        protocols.toArray(new String[protocols.size()]),
        cipherSuites,
        hostnameVerifier);
    return HttpClients.custom().setSSLSocketFactory(factory).build();
  }

  public void hostnameVerifier(HostnameVerifier hostnameVerifier) {
    this.hostnameVerifier = hostnameVerifier;
  }

  public void setCipherSuites(String[] cipherSuites) {
    this.cipherSuites = cipherSuites;
  }

  public void enableTLS10() {
    protocols.add(TLS_VERSION_1);
  }

  public void enableSSL3() {
    protocols.add(SSL_VERSION_3);
  }

  public void disableTLS11() {
    protocols.remove(TLS_VERSION_1_1);
  }

  public void disableTLS12() {
    protocols.remove(TLS_VERSION_1_2);
  }

}
