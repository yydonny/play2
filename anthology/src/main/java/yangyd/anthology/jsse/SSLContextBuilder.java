package yangyd.anthology.jsse;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import yangyd.anthology.misc.Utils;

import javax.net.ssl.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.ListIterator;

/**
 * <p>Simplified builder for both the Netty {@link io.netty.handler.ssl.SslContext} and the JSSE {@link javax.net.ssl.SSLContext}.</p>
 * <p>By default the built SSL context skips server certificate verification (not secure). provide a {@code trustKeyStore} and implement {@code trustStrategy} to override.</p>
 * <p>2-way SSL handshake can be enabled by providing {@code clientKeyStore} and {@code clientKeyPass}</p>
 */
public class SSLContextBuilder {
  // Client-side certificate for 2-way SSL verification
  private byte[] clientKeyStore;
  private String clientKeyPass = "";

  // Server-side certificate and verification handling
  private byte[] trustKeyStore;
  private TrustStrategy trustStrategy = Defaults.INSECURE_TRUST_STRATEGY;

  private String randomSource = Defaults.RANDOM_SOURCE;

  /**
   * @return Netty {@link SslContext}
   */
  public SslContext buildForNetty() {
    SslContextBuilder builder = SslContextBuilder.forClient();
    try {
      if (clientKeyStore != null) {
        builder.keyManager(keyManagerFactory(clientKeyStore, clientKeyPass.toCharArray()));
      }
      builder.trustManager(trustKeyStore == null ?
          InsecureTrustManagerFactory.INSTANCE : trustManagerFactory(trustKeyStore));

      return builder.build();
    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * @return JSSE {@link SSLContext}.
   */
  public SSLContext buildForJSSE() {
    try {
      SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(keyManagers(), trustManagers(), SecureRandom.getInstance(randomSource));
      return sslContext;
    } catch (NoSuchAlgorithmException | KeyManagementException | CertificateException |
        UnrecoverableKeyException | IOException | KeyStoreException e) {
      throw new RuntimeException(e);
    }
  }

  private KeyManager[] keyManagers() throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, UnrecoverableKeyException {
    return clientKeyStore == null ?
        null : keyManagerFactory(clientKeyStore, clientKeyPass.toCharArray()).getKeyManagers();
  }

  private TrustManager[] trustManagers() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
    if (trustKeyStore == null) {
      return InsecureTrustManagerFactory.INSTANCE.getTrustManagers();

    } else {
      TrustManager[] managers = trustManagerFactory(trustKeyStore).getTrustManagers();
      ListIterator<TrustManager> iterator = Arrays.asList(managers).listIterator();
      while (iterator.hasNext()) {
        TrustManager trustManager = iterator.next();
        if (trustManager instanceof X509TrustManager) {
          iterator.set(new StrategyBasedTrustManager((X509TrustManager) trustManager, trustStrategy));
        }
      }
      return managers;
    }
  }

  private static KeyManagerFactory keyManagerFactory(byte[] keyStore, char[] keyPass)
      throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, UnrecoverableKeyException
  {
    KeyManagerFactory factory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    factory.init(loadKeyStore(keyStore, keyPass), keyPass);
    return factory;
  }

  private static TrustManagerFactory trustManagerFactory(byte[] keyStore)
      throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException
  {
    TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    factory.init(loadKeyStore(keyStore, new char[0])); // no password for public keys
    return factory;
  }

  /**
   * Load the key store JKS for client certificates.
   * @param input
   * @throws IOException
   */
  public void loadClientKeyStore(InputStream input) throws IOException {
    clientKeyStore = readBytes(input);
  }

  private static byte[] readBytes(InputStream input) throws IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    Utils.pump(buffer, input);
    return buffer.toByteArray();
  }

  /**
   * @param clientKeyPass Keystore passphrase of the client private key
   */
  public void setClientKeyPass(String clientKeyPass) {
    this.clientKeyPass = clientKeyPass;
  }

  /**
   * Load the key store JKS for server certificates.
   * @param input
   * @throws IOException
   */
  public void setTrustKeyStore(InputStream input) throws IOException {
    trustKeyStore = readBytes(input);
  }

  /**
   * @param randomSource Hardware random source.
   * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#SecureRandom">SecureRandom</a>
   */
  public void setRandomSource(String randomSource) {
    this.randomSource = randomSource;
  }

  /**
   * @param trustStrategy the callback to determine whether server certificate is accepted. By default the server certificate is not verified.
   */
  public void trustStrategy(TrustStrategy trustStrategy) {
    this.trustStrategy = trustStrategy;
  }

  private static KeyStore loadKeyStore(byte[] bytes, char[] keyPass)
      throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException
  {
    KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
    keyStore.load(new ByteArrayInputStream(bytes), keyPass);
    return keyStore;
  }

  private interface Defaults {
    String RANDOM_SOURCE = "\\".equals(System.getProperty("file.separator")) ? // dirty way to determine the platform
        "Windows-PRNG" : "NativePRNGNonBlocking";
    TrustStrategy INSECURE_TRUST_STRATEGY = (chain, authType) -> true;
  }

}
