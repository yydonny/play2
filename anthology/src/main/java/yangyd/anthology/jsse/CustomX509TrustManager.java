package yangyd.anthology.jsse;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Example of a custom X509TrustManager.
 *
 * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/security/jsse/JSSERefGuide.html#TrustManagerFactory">Creating Your Own X509TrustManager</a>
 */
public class CustomX509TrustManager implements X509TrustManager {
  // The default PKIX X509TrustManager. Decisions are delegated to it, and a fall back is performed
  // if the default X509TrustManager does not trust it.
  private final X509TrustManager pkixTrustManager;

  public CustomX509TrustManager(TrustManagerFactory trustManagerFactory) {
    for (TrustManager tm : trustManagerFactory.getTrustManagers()) { // look for the first X509TrustManager instance
      if (tm instanceof X509TrustManager) {
        pkixTrustManager = (X509TrustManager) tm;
        return;
      }
    }
    throw new RuntimeException("No default X509TrustManager available.");
  }

  /*
   * Delegate to the default trust manager.
   */
  public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    try {
      pkixTrustManager.checkClientTrusted(chain, authType);
    } catch (CertificateException e) {
      // do any special handling here, or rethrow exception.
    }
  }

  /*
   * Delegate to the default trust manager.
   */
  public void checkServerTrusted(X509Certificate[] chain, String authType)
      throws CertificateException {
    try {
      pkixTrustManager.checkServerTrusted(chain, authType);
    } catch (CertificateException e) {
      // Possibly pop up a dialog box asking whether to trust the cert chain.
    }
  }

  /*
   * Merely pass this through.
   */
  public X509Certificate[] getAcceptedIssuers() {
    return pkixTrustManager.getAcceptedIssuers();
  }
}
