package yangyd.anthology.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletRequestAttributes;
import yangyd.anthology.web.model.ErrorResponse;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Render error information gathered by Spring Boot for unhandled exception.
 * @see AbstractErrorController
 */
@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
class GenericErrorController implements ErrorController {
  private static final Logger logger = LoggerFactory.getLogger(GenericErrorController.class);
  private static final String SERVLET_STATUS_CODE = "javax.servlet.error.status_code";
  private static final Pattern MESSAGE_DELIMITER = Pattern.compile(":\\s?");
  private final ErrorAttributes errorAttributes;

  @NotNull
  @Value("${server.error.path:${error.path:/error}}")
  private String errorPath;

  @Autowired
  GenericErrorController(ErrorAttributes errorAttributes) {
    this.errorAttributes = errorAttributes;
  }

  @Override
  public String getErrorPath() {
    return errorPath;
  }

  @RequestMapping
  @ResponseBody
  ResponseEntity<ErrorResponse> renderError(HttpServletRequest request) {
    // @see org.springframework.boot.autoconfigure.web.DefaultErrorAttributes
    int status = statusCode(request);
    Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(new ServletRequestAttributes(request), false);

    // Extract brief message (the message is concatenated of all exception causes in stacktrace)
    String[] messages = MESSAGE_DELIMITER.split(String.valueOf(errorInfo.get("message")));

    ErrorResponse errorResponse;
    if (status >= 500) {
      logError(errorInfo);
      errorResponse = new ErrorResponse("SERVER_ERROR", messages[0]);
    } else {
      errorResponse = new ErrorResponse("INVALID_REQUEST", messages[0]);
    }
    if (messages.length >= 2) {
      errorResponse.setDetail(filtered(messages[1]));
    }
    return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(status));
  }

  /**
   * Filtering message for particular cases (i.e. masking sensitive information)
   */
  private String filtered(String message) {
    return message;
  }

  private void logError(Map<String, Object> error) {
    logger.error("Unhandled Exception in {} - {}: {}", error.get("path"), error.get("exception"), error.get("message"));
  }

  private int statusCode(HttpServletRequest request) {
    Integer statusCode = (Integer) request.getAttribute(SERVLET_STATUS_CODE);
    if (statusCode == null) {
      statusCode = 500;
    }
    return statusCode;
  }
}
