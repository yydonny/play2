package yangyd.anthology.web;

import org.springframework.core.io.InputStreamResource;

import java.io.IOException;
import java.io.InputStream;

/**
 * Customized InputStreamResource that set filename and (empty) content-length properly.
 * Used when sending multipart/form-data request with RestTemplate.
 */
public class NamedInputStreamResource extends InputStreamResource {
  private final String fileName;

  public NamedInputStreamResource(InputStream in, String fileName) {
    super(in);
    this.fileName = fileName;
  }

  @Override
  public String getFilename() {
    return fileName;
  }

  @Override
  public long contentLength() throws IOException {
    // Return -1 so that ResourceHttpMessageConverter don't attempt to set Content-Length.
    // @see org.springframework.http.converter.ResourceHttpMessageConverter.getContentLength()
    return -1;
  }

}
