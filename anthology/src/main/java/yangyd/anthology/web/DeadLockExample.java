package yangyd.anthology.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.InputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;

@RestController
@Profile("example-only")
class DeadLockExample {
  private final RestTemplate restTemplate;

  @Autowired
  public DeadLockExample(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @RequestMapping("/deadlock")
  public Callable<StreamingResponseBody> deadlock() {
    String url = "some backend data source";
    Response response = new Response();
    restTemplate.execute(url, HttpMethod.GET, null, incoming -> {
      InputStream inputStream = incoming.getBody();
      CountDownLatch latch = new CountDownLatch(1);
      response.fullfill(outputStream -> { // StreamingResponseBody#writeTo(), called by Spring (which never happens)
        // transfering data from input to output......
        latch.countDown();
      }); // Output Stream closing

      try {
        latch.await(); // <--------------------------- deadlock here
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return null;
    }); // RestTemplate.execute will not return until response callback returns,
    // which in turn is waiting for the StreamingResponseBody#writeTo() being called.

    // never reaches here
    return response;
  }

  private static class Response implements Callable<StreamingResponseBody> {
    private final BlockingQueue<StreamingResponseBody> data = new LinkedBlockingDeque<>();
    void fullfill(StreamingResponseBody body) {
      data.offer(body);
    }
    @Override
    public StreamingResponseBody call() throws Exception {
      return data.take();
    }
  }

}
