package yangyd.anthology.web.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Generic error response for restful APIs.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
  private final String code;
  private String reason;
  private String detail;

  public ErrorResponse(String code) {
    this.code = code;
  }

  public ErrorResponse(String code, String reason) {
    this.code = code;
    this.reason = reason;
  }

  public ErrorResponse(String code, String reason, String detail) {
    this.code = code;
    this.reason = reason;
    this.detail = detail;
  }

  public String getCode() {
    return code;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }
}
