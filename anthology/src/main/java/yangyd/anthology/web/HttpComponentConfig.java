package yangyd.anthology.web;

import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * An example of building customized restTemplate with apache http-client
 *   - insecureRestTemplate that ignores SSL verification
 *   - proxiedRestTemplate that send request via proxy
 */
@Profile("example-only")
@Configuration
public class HttpComponentConfig {

  @Value("${http.proxy.host}")
  String proxyHost;
  @Value("${http.proxy.port}")
  int proxyPort;

  @Bean
  RestTemplate insecureRestTemplate() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    return new RestTemplate(requestFactory(insecureHttpClient()));
  }

  private SSLContext noCheckSSLContext() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    return SSLContexts.custom().loadTrustMaterial(null, (c, a) -> true).build();
  }

  private CloseableHttpClient insecureHttpClient() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    return HttpClients.custom()
        .setSSLSocketFactory(new SSLConnectionSocketFactory(noCheckSSLContext()))
        .build();
  }

  @Bean
  RestTemplate proxiedRestTemplate() {
    return new RestTemplate(requestFactory(proxyClient()));
  }

  private CloseableHttpClient proxyClient() {
    return HttpClients.custom()
        .setProxy(new HttpHost(proxyHost, proxyPort, "http"))
        .build();
  }

  private HttpComponentsClientHttpRequestFactory requestFactory(CloseableHttpClient httpClient) {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
    factory.setHttpClient(httpClient);
    return factory;
  }
}
