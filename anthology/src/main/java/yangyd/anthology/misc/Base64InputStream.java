package yangyd.anthology.misc;

import java.io.IOException;
import java.io.InputStream;

public class Base64InputStream extends InputStream {
  private final Base64Buffer buffer;

  public Base64InputStream(InputStream inputStream) {
    this.buffer = Base64Buffer.netty(inputStream);
  }

  @Override
  public int read() throws IOException {
    buffer.fill();
    return buffer.available() > 0 ? buffer.get() : -1;
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    buffer.fill();
    return buffer.available() > 0 ? buffer.get(b, off, len) : -1;
  }

  @Override
  public long skip(long n) throws IOException {
    throw new UnsupportedOperationException("skip() is not supported");
  }

  @Override
  public int available() throws IOException {
    return buffer.available();
  }

  @Override
  public void close() throws IOException {
    buffer.close();
  }

  @Override
  public synchronized void mark(int readlimit) {
    throw new UnsupportedOperationException("mark() is not supported");
  }

  @Override
  public synchronized void reset() throws IOException {
    throw new UnsupportedOperationException("reset() is not supported");
  }

  @Override
  public boolean markSupported() {
    return super.markSupported();
  }
}
