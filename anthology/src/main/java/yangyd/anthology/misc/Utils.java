package yangyd.anthology.misc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

public class Utils {

  private static final ObjectMapper mapper = new ObjectMapper()
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false)
      .configure(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS, false);

  private static final ObjectWriter pretty = mapper.writerWithDefaultPrettyPrinter();
  private static final ObjectWriter compact = mapper.writer();

  private static final Pattern CRLF = Pattern.compile("\r?\n");

  public static String oneLine(String s) {
    return CRLF.matcher(s).replaceAll(" ");
  }

  public static String json(Object o) {
    try {
      return compact.writeValueAsString(o);
    } catch (JsonProcessingException e) {
      return "\"(unserializable object)\"";
    }
  }

  public static String prettyJson(Object o) {
    try {
      return pretty.writeValueAsString(o);
    } catch (JsonProcessingException e) {
      return "\"(unserializable object)\"";
    }
  }

  /**
   * Pump data from input to output stream.
   * @param out
   * @param in
   * @throws IOException
   */
  public static void pump(OutputStream out, InputStream in) throws IOException {
    byte[] buf = new byte[16 * 1024];
    int n;
    while ((n = in.read(buf)) > -1) out.write(buf, 0, n);
  }

  public static File tempDir() {
    return new File(System.getProperty("java.io.tmpdir"));
  }
}
