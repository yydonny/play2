package yangyd.anthology.misc.buffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteBuffer;

public class NettyBuffer implements UnboundedBuffer {
  private final ByteBuf buffer = Unpooled.buffer();

  @Override
  public void append(ByteBuffer byteBuffer) {
    buffer.writeBytes(byteBuffer);
  }

  @Override
  public int get(byte[] dst, int offset, int len) {
    int read = Math.min(len, buffer.readableBytes());
    if (read > 0) {
      buffer.readBytes(dst, offset, read);
    }
    return read;
  }

  @Override
  public byte get() {
    return buffer.readByte();
  }

  @Override
  public int available() {
    return buffer.readableBytes();
  }

  @Override
  public void clear() {
    buffer.clear();
  }
}
