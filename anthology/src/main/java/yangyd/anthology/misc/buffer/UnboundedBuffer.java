package yangyd.anthology.misc.buffer;

import java.nio.ByteBuffer;

public interface UnboundedBuffer {
  void append(ByteBuffer byteBuffer);

  /**
   * Conduct a single bulk read from this buffer. The behavior is generally the same as {@link java.io.InputStream#read(byte[], int, int)},
   * but the returned value is always non-negative, as there's no concept of "EOF" for unbounded buffer.
   * @param dst
   * @param offset
   * @param len
   * @return number of bytes read. Always non-negative. Zero means no data available at the moment.
   */
  int get(byte[] dst, int offset, int len);

  /**
   * Read 1 byte from the buffer.
   * @return the byte read.
   * @throws IndexOutOfBoundsException if no data available at the time of read.
   */
  byte get();

  int available();

  /**
   * Discard all data in the buffer.
   */
  void clear();
}
