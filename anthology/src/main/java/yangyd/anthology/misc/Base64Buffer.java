package yangyd.anthology.misc;

import yangyd.anthology.misc.buffer.NettyBuffer;
import yangyd.anthology.misc.buffer.SimpleUnboundedBuffer;
import yangyd.anthology.misc.buffer.UnboundedBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Base64;

/**
 * A buffer that pulls and base64-encodes data from the source {@link InputStream}.
 * <b>NOT thread-safe.</b>
 */
class Base64Buffer {
  private static final Base64.Encoder base64Encoder = Base64.getEncoder();
  private static final int READ_BUF_SIZE = 3 * 16 * 1024;
  private final InputStream source;
  private final UnboundedBuffer buffer;

  private final byte[] readBuf = new byte[READ_BUF_SIZE];
  private boolean drained = false;
  private int remainder = 0;

  public static Base64Buffer netty(InputStream inputStream) {
    return new Base64Buffer(inputStream, new NettyBuffer());
  }

  public static Base64Buffer queued(InputStream inputStream) {
    return new Base64Buffer(inputStream, new SimpleUnboundedBuffer());
  }

  private Base64Buffer(InputStream source, UnboundedBuffer buffer) {
    this.source = source;
    this.buffer = buffer;
  }

  /**
   * Do a single read on the source {@link InputStream}, then encode and store read data in the {@code UnboundedBuffer}.
   * This method returns immediately if the source is already drained, otherwise it may block while reading from the source.
   * @return Whether the source is drained after this read.
   * @throws IOException
   */
  public boolean fill() throws IOException {
    if (!drained) {
      int nRead = source.read(readBuf, remainder, READ_BUF_SIZE - remainder);
      if (nRead < 0) { // hit EOF of the source, encode the remainder of data
        encodeBuffer(remainder);
        remainder = 0;
        drained = true;
      } else {
        nRead += remainder; // include remainder bytes from last read
        int nEncode = trim3(nRead); // Trim to multiplies of 3 so that encoded base64 chunk can be concatenated
        encodeBuffer(nEncode); // encode and save the chunk
        remainder = nRead - nEncode;
        rebase(readBuf, nEncode, remainder); // move possible remainder bytes to the beginning of readBuf
      }
    }
    return drained;
  }

  public void close() throws IOException {
    drained = true;
    source.close();
  }

  public int available() {
    return buffer.available();
  }

  public int get(byte[] dst, int offset, int len) {
    return buffer.get(dst, offset, len);
  }

  public byte get() {
    return buffer.get();
  }

  /**
   * Base64 encode the first {@code len} bytes of the {@code readBuf}, and append the encoded data to the buffer queue.
   */
  private void encodeBuffer(int len) {
    if (len > 0) {
      buffer.append(base64Encoder.encode(ByteBuffer.wrap(readBuf, 0, len)));
    }
  }

  /**
   * put {@code len} bytes from {@code start} to the beginning of the {@code buf}.
   * Only handles {@code len} of 0, 1, or 2.
   */
  private static void rebase(byte[] buf, int start, int len) {
    switch (len) {
      case 2:
        buf[0] = buf[start];
        buf[1] = buf[start + 1];
        break;
      case 1:
        buf[0] = buf[start];
        break;
      case 0:
        break;
      default:
        throw new IllegalArgumentException("len must be 0, 1 or 2");
    }
  }

  private static int trim3(int n) {
    return n % 3 == 0 ? n : (n / 3) * 3;
  }
}
