package yangyd.anthology.misc.buffer

import java.nio.ByteBuffer

import scala.annotation.tailrec

class SimpleUnboundedBuffer extends UnboundedBuffer {
  var cache: Seq[ByteBuffer] = Nil

  /**
    * Move at most `total` bytes from `bufs` to `dst`.
    * This is a recursive method and initial call must ensure `toMove` == `total`
    * @param bufs source of data move
    * @param toMove number of bytes left to move
    * @param dst destination of data move
    * @param offset where to start write in dst
    * @param total total number of bytes to move. Will be passed unchanged through recursive.
    * @return actual bytes moved
    */
  @tailrec
  private def moveBytes(bufs: Seq[ByteBuffer], toMove: Int, dst: Array[Byte], offset: Int, total: Int): Int = bufs match {
    case buf :: rest ⇒ // at least 1 ByteBuffer left
      if (buf.hasRemaining) {
        val nRead = buf.remaining min toMove
        buf.get(dst, offset, nRead)
        if (nRead == toMove) { // have moved enough bytes, finish
          total
        } else { // more bytes to move, continue
          moveBytes(bufs, toMove - nRead, dst, offset + nRead, total)
        }
      } else { // head drained, shift to next ByteBuffer
        moveBytes(rest, toMove, dst, offset, total)
      }
    case _ ⇒ total - toMove // no more bytes in cache, stop
  }

  override def available(): Int = cache.map(_.remaining).sum

  override def get(dst: Array[Byte], offset: Int, len: Int): Int = moveBytes(cache, len, dst, offset, len)

  override def get(): Byte = cache.find(_.hasRemaining) match {
    case Some(buf) ⇒ buf.get()
    case None ⇒ throw new IndexOutOfBoundsException
  }

  override def append(byteBuffer: ByteBuffer): Unit = {
    cache = cache.filter(_.hasRemaining) :+ byteBuffer
  }

  override def clear(): Unit = cache = Nil
}
