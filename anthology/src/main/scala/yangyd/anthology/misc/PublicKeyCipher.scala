package yangyd.anthology.misc

import java.math.BigInteger
import java.security.KeyFactory
import java.security.spec.{KeySpec, RSAPublicKeySpec}
import javax.crypto.Cipher

object PublicKeyCipher {
  private val RSA_ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding"
  private val RSA_KEY_FACTORY = KeyFactory.getInstance("RSA")
  private val HEX_CHAR: Array[Char] = "0123456789ABCDEF".toCharArray

  private def cipher(keySpec: KeySpec): Cipher = {
    val cipher = Cipher.getInstance(RSA_ECB_PKCS1_PADDING)
    cipher.init(Cipher.ENCRYPT_MODE, RSA_KEY_FACTORY.generatePublic(keySpec))
    cipher
  }

  private def hex(input: Array[Byte]): String = {
    val hexString: StringBuilder = new StringBuilder(2 * input.length)
    for (b ← input) {
      hexString.append(HEX_CHAR((b >> 4) & 0xF))
      hexString.append(HEX_CHAR(b & 0xF))
    }
    hexString.toString()
  }
}

/**
  * RSA public key cipher, used for encryption.
  * @param mod the modulus of RSA public key.
  * @param exp the exponent of RSA public key.
  */
case class PublicKeyCipher(mod: String, exp: String) {
  import PublicKeyCipher._
  private val keySpec = new RSAPublicKeySpec(new BigInteger(mod, 16), new BigInteger(exp, 16))

  /**
    * Encrypt message with this cipher.
    * @param message
    * @return encrypted data as hex string.
    */
  def encrypt(message: Array[Byte]): String = hex(cipher(keySpec).doFinal(message))
}
